/*
 * 1. Finnið nöfn og heimilisföng allra ofurhetja sem hanga á barnum "The Prancing Pony".
 */
SELECT
    DISTINCT d.name,
    d.addr
FROM
    drinkers d
    JOIN frequents f ON f.drinker = d.name
WHERE
    f.bar = 'The Prancing Pony';


/*
 * 2: Skrifið sql skipun sem finnur allar ofurhetjur sem búa vestan megin á manhattan og hanga á einhverjum bar sem kemur fyrir í einhverjum af "Elder Scrolls" tölvuleikjunum. Sýnið nafn ofurhetjunnar, nafnið á barnum og addressuna á barnum (undir heitunum Superhero, Bar og Game).
 */
SELECT
    DISTINCT d.name AS Superhero,
    b.name as Bar,
    b.addr as Game
FROM
    drinkers d
    JOIN frequents f ON f.drinker = d.name
    JOIN bars b ON b.name = f.bar
WHERE
    b.addr LIKE '%Elder Scrolls%'
    AND d.addr LIKE '%West%';

/*
 * 3: Í þessum ofurhetjuheimi þá er vinsælt að fá sér svokallað “double-trouble” sem eru tveir bjórar frá sama framleiðanda.  Skrifið sql skipun sem finnur alla möguleika fyrir svona “double-trouble” og birtið nafn framleiðanda og nafn bjóranna tveggja, raðað í stafrófsröð eftir nafni framleiðandans, nafn fyrra bjórsins og svo nafni seinna bjórsins.  Hver “double-trouble” möguleiki á aðeins að koma fyrir einu sinni.
 */
SELECT
    DISTINCT b1.manf,
    b1.name,
    b2.name
FROM
    beers b1
    JOIN beers b2 ON b2.manf = b1.manf
WHERE
    b1.name < b2.name
ORDER BY
    b1.manf,
    b1.name,
    b2.name;

/*
 * 4: Finnið meðalverð, hæsta verð og lægsta verð á þeim bjórum sem framleiddir eru af brugghúsi sem kallar sig eitthvað eins og “Brewing Company” eða “Brewing Co.”.
 */
SELECT
    AVG(s.price),
    MAX(s.price),
    MIN(s.price)
FROM
    sells s
    JOIN beers b ON b.name = s.beer
WHERE
    b.manf LIKE '%Brewing Co%';

/*
 * 5: Finnið dýrasta “Imperial” bjórinn og sýnið nafn bjórsins, framleiðanda, nafn barsins sem selur bjórinn á þessu verði og heimilisfang barsins.
 */
SELECT
    be.name,
    be.manf,
    ba.name,
    ba.addr
FROM
    beers be
    JOIN sells s ON s.beer = be.name
    JOIN bars ba ON ba.name = s.bar
WHERE
    be.name LIKE '%Imperial%'
    AND s.price = (SELECT
                       MAX(s1.price)
                   FROM
                       sells s1
                   WHERE
                       s1.beer LIKE '%Imperial%');

/*
 * 6: Finnið öll brugghús sem eru ekki seld á neinum bar og birtið nöfn brugghússins og nöfn þeirra bjóra sem þessi brugghús framleiða, raðað í stafrófsröð eftir nafni brugghúss og svo bjórs.
 */
SELECT
    DISTINCT b.manf,
    b.name
FROM
    beers b
WHERE
    0 = (SELECT
             COUNT(*)
         FROM
             sells s
             JOIN beers b1 ON b1.name = s.beer
         WHERE
             b1.manf = b.manf);

/*
 * 7: Finnið nöfn og heimilisföng allra ofurhetja sem eiga heima neðar en 100 stræti og fíla bjór sem búinn er til af Russian River Brewing Company.  Hver ofurhetja má aðeins koma einu sinni fyrir í listanum. (hint: göturnar eru frá 4th og upp að 160th Street, þ.e. hæsta gatan er 160th.).  Raðið ofurhetjunum upp í stafrófsröð.
 */
SELECT
    DISTINCT d.name,
    d.addr
FROM
    drinkers d
    JOIN likes l ON l.drinker = d.name
    JOIN beers b ON b.name = l.beer
WHERE
    b.manf = 'Russian River Brewing Company'
    AND d.addr ~ '\y\y(\d|\d\d)th\y'
ORDER BY
    d.name;

/*
 * 8: Búið til lista sem sýnir 5 vinsælustu IPA bjórana, þ.e. hvaða IPA bjórar eru oftast valdir þegar við skoðum hvaða bjór ofurhetjurnar fíla.
 */
SELECT
    l.beer,
    COUNT(*) as drinkers
FROM
    likes l
WHERE
    l.beer LIKE '%IPA%'
GROUP BY
    l.beer
ORDER BY
    drinkers DESC
LIMIT 5;

/*
 * 9: Búið til lista sem sýnir 10 algengustu bjórframleiðendur, þ.e. hvaða bjórframleiðendur eru seldir á sem flestum stöðum.  Ef sami framleiðandi er með fleiri en einn bjór á einhverjum bar þá á sá bar samt aðeins að vera talinn einu sinni.
 */
SELECT
    b.manf,
    COUNT(distinct s.bar) as bars
FROM
    beers b
    JOIN sells s ON s.beer = b.name
GROUP BY
    b.manf
ORDER BY
    bars DESC
LIMIT 10;
