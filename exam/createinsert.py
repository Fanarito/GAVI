import csv

text = ""

people = set()
phones = set()

callers = set()
recievers = set()


def create_person_insert(name):
    return "INSERT INTO Persons(person) VALUES ('{}');\n".format(name)


def create_phone_insert(phone):
    return "INSERT INTO Phones(phone) VALUES ('{}');\n".format(phone)


def create_call_insert(row):
    return """INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('{}', '{}', '{}', {});\n""".format(row['call_from'],
                                           row['call_to'],
                                           row['phone_type'],
                                           row['length'])


filename = 'phonecalls.csv'
with open(filename) as csvfile:
    sniffer = csv.Sniffer()
    dialect = sniffer.sniff(csvfile.read(1024))
    csvfile.seek(0)
    reader = csv.DictReader(csvfile, dialect=dialect)

    phone_calls = ""
    for row in reader:
        people.add(row['call_from'])
        people.add(row['call_to'])
        phones.add(row['phone_type'].split()[0])
        phone_calls += create_call_insert(row)

        recievers.add(row['call_to'])
        callers.add(row['call_from'])

    for person in people:
        text += create_person_insert(person)
    for phone in phones:
        text += create_phone_insert(phone)
    text += phone_calls


only_callers = set()
only_recievers = set()

for person in people:
    if person not in recievers:
        only_callers.add(person)
    elif person not in callers:
        only_recievers.add(person)

print('Only callers:')
print(', '.join(sorted(only_callers)))
print('Only recievers:')
print(', '.join(sorted(only_recievers)))


outname = 'insertstatements.sql'
with open(outname, mode='w') as out:
    out.write(text)
