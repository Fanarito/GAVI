SELECT * FROM Phones;


SELECT
    call_to,
    COUNT(*) as calls
FROM
    Phonecalls
WHERE
    phone_type = 'iPhone'
GROUP BY
    call_to
ORDER BY
    calls DESC;


SELECT
    call_from,
    COUNT(*) as calls,
    SUM(length) as total_length
FROM
    Phonecalls
GROUP BY
    call_from
HAVING
    SUM(length) = (SELECT SUM(length) as total
                   FROM Phonecalls
                   GROUP BY call_from
                   ORDER BY total DESC
                   LIMIT 1);


SELECT
    p.person,
    COUNT(call_from) as total_calls,
    SUM(length) as total_length
FROM
    Persons p
    LEFT JOIN Phonecalls pc ON pc.call_from = p.person
GROUP BY
    p.person
ORDER BY
    total_calls DESC,
    p.person;


SELECT
    p.person as name,
    average_from as from,
    average_to as to,
    average_other as other
FROM
    Persons p
    JOIN (SELECT pc1.call_from, AVG(length) as average_from
          FROM Phonecalls pc1
          GROUP BY pc1.call_from) f ON f.call_from = p.person
    JOIN (SELECT pc2.call_to, AVG(length) as average_to
          FROM Phonecalls pc2
          GROUP BY pc2.call_to) t ON t.call_to = p.person
    JOIN (SELECT p1.person, AVG(length) as average_other
          FROM
              Persons p1
              JOIN Phonecalls pc3 ON p1.person <> pc3.call_from and p1.person <> pc3.call_to
          GROUP BY p1.person) o ON o.person = p.person;
