CREATE TABLE Persons (
    person VARCHAR(250) PRIMARY KEY
);

CREATE TABLE Phones (
    phone VARCHAR(150) PRIMARY KEY
);

CREATE TABLE Phonecalls (
    id SERIAL PRIMARY KEY,
    call_from VARCHAR(250) REFERENCES Persons(person),
    call_to VARCHAR(250) REFERENCES Persons(person),
    phone_type VARCHAR(250),
    length INTEGER
);
