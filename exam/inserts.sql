INSERT INTO Persons(person) VALUES ('Ivory');
INSERT INTO Persons(person) VALUES ('Kandace');
INSERT INTO Persons(person) VALUES ('Holley');
INSERT INTO Persons(person) VALUES ('Leana');
INSERT INTO Persons(person) VALUES ('Genie');
INSERT INTO Persons(person) VALUES ('Darius');
INSERT INTO Persons(person) VALUES ('Jene');
INSERT INTO Persons(person) VALUES ('Gwyneth');
INSERT INTO Persons(person) VALUES ('Don');
INSERT INTO Persons(person) VALUES ('Delsie');
INSERT INTO Persons(person) VALUES ('Caryl');
INSERT INTO Persons(person) VALUES ('Maryann');
INSERT INTO Persons(person) VALUES ('Ramon');
INSERT INTO Persons(person) VALUES ('Otis');
INSERT INTO Persons(person) VALUES ('Elinore');
INSERT INTO Persons(person) VALUES ('Jeffrey');
INSERT INTO Persons(person) VALUES ('Charlsie');
INSERT INTO Persons(person) VALUES ('Henry');
INSERT INTO Persons(person) VALUES ('Kathleen');
INSERT INTO Persons(person) VALUES ('Mose');
INSERT INTO Persons(person) VALUES ('Deon');
INSERT INTO Persons(person) VALUES ('Pamila');
INSERT INTO Persons(person) VALUES ('Mignon');
INSERT INTO Persons(person) VALUES ('Asha');
INSERT INTO Persons(person) VALUES ('Antonio');
INSERT INTO Persons(person) VALUES ('Chang');
INSERT INTO Persons(person) VALUES ('Jermaine');
INSERT INTO Persons(person) VALUES ('Maude');
INSERT INTO Persons(person) VALUES ('Fredericka');
INSERT INTO Persons(person) VALUES ('Julianna');
INSERT INTO Persons(person) VALUES ('Gregorio');
INSERT INTO Persons(person) VALUES ('Tawna');
INSERT INTO Persons(person) VALUES ('Elsa');
INSERT INTO Persons(person) VALUES ('Frederick');
INSERT INTO Persons(person) VALUES ('Cyril');
INSERT INTO Persons(person) VALUES ('Teri');
INSERT INTO Persons(person) VALUES ('Leandro');
INSERT INTO Persons(person) VALUES ('Sydney');
INSERT INTO Persons(person) VALUES ('Antoine');
INSERT INTO Persons(person) VALUES ('Jaime');
INSERT INTO Persons(person) VALUES ('Esmeralda');
INSERT INTO Persons(person) VALUES ('Linette');
INSERT INTO Persons(person) VALUES ('Columbus');
INSERT INTO Persons(person) VALUES ('Claud');
INSERT INTO Persons(person) VALUES ('Darnell');
INSERT INTO Persons(person) VALUES ('Kathaleen');
INSERT INTO Persons(person) VALUES ('Lenny');
INSERT INTO Persons(person) VALUES ('Jacques');
INSERT INTO Persons(person) VALUES ('Pasquale');
INSERT INTO Persons(person) VALUES ('Phebe');
INSERT INTO Persons(person) VALUES ('Richard');
INSERT INTO Persons(person) VALUES ('Sanford');
INSERT INTO Persons(person) VALUES ('Annita');
INSERT INTO Persons(person) VALUES ('Savannah');
INSERT INTO Persons(person) VALUES ('Cole');
INSERT INTO Persons(person) VALUES ('Burl');
INSERT INTO Persons(person) VALUES ('Andreas');
INSERT INTO Persons(person) VALUES ('Margaret');
INSERT INTO Persons(person) VALUES ('Roseline');
INSERT INTO Persons(person) VALUES ('Shantel');
INSERT INTO Persons(person) VALUES ('Rocco');
INSERT INTO Persons(person) VALUES ('Roselle');
INSERT INTO Persons(person) VALUES ('Reggie');
INSERT INTO Persons(person) VALUES ('Josiah');
INSERT INTO Persons(person) VALUES ('Andre');
INSERT INTO Persons(person) VALUES ('Cuc');
INSERT INTO Persons(person) VALUES ('Tyrone');
INSERT INTO Persons(person) VALUES ('Doretta');
INSERT INTO Persons(person) VALUES ('Wyatt');
INSERT INTO Persons(person) VALUES ('Raguel');
INSERT INTO Persons(person) VALUES ('Deloise');
INSERT INTO Persons(person) VALUES ('Dale');
INSERT INTO Persons(person) VALUES ('Suzie');
INSERT INTO Persons(person) VALUES ('Katie');
INSERT INTO Persons(person) VALUES ('Lillie');
INSERT INTO Persons(person) VALUES ('Dana');
INSERT INTO Persons(person) VALUES ('Ahmad');
INSERT INTO Persons(person) VALUES ('Howard');
INSERT INTO Persons(person) VALUES ('Heather');
INSERT INTO Persons(person) VALUES ('Avelina');
INSERT INTO Persons(person) VALUES ('Neal');
INSERT INTO Persons(person) VALUES ('Tad');
INSERT INTO Persons(person) VALUES ('Jarrett');
INSERT INTO Persons(person) VALUES ('Freddy');
INSERT INTO Persons(person) VALUES ('Alison');
INSERT INTO Persons(person) VALUES ('Essie');
INSERT INTO Persons(person) VALUES ('Jess');
INSERT INTO Persons(person) VALUES ('Keneth');
INSERT INTO Persons(person) VALUES ('Anthony');
INSERT INTO Persons(person) VALUES ('Alexia');
INSERT INTO Persons(person) VALUES ('Grant');
INSERT INTO Persons(person) VALUES ('Edwardo');
INSERT INTO Persons(person) VALUES ('Synthia');
INSERT INTO Persons(person) VALUES ('Latonia');
INSERT INTO Persons(person) VALUES ('Lelah');
INSERT INTO Persons(person) VALUES ('Tamar');
INSERT INTO Persons(person) VALUES ('Hung');
INSERT INTO Persons(person) VALUES ('Gemma');
INSERT INTO Persons(person) VALUES ('Delinda');
INSERT INTO Persons(person) VALUES ('Kristie');
INSERT INTO Phones(phone) VALUES ('moto');
INSERT INTO Phones(phone) VALUES ('Sony');
INSERT INTO Phones(phone) VALUES ('LG');
INSERT INTO Phones(phone) VALUES ('iPhone');
INSERT INTO Phones(phone) VALUES ('HTC');
INSERT INTO Phones(phone) VALUES ('Nokia');
INSERT INTO Phones(phone) VALUES ('Samsung');
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Avelina', 'iPhone 6', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Genie', 'Samsung S8', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Tad', 'HTC Bolt', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Antonio', 'Sony Xperia R1', 201);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Delinda', 'iPhone 8', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Tamar', 'Sony Xperia', 396);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Tyrone', 'Samsung Galaxy J5', 545);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Caryl', 'Samsung J7 Pro', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Heather', 'moto x4', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Darnell', 'LG G6', 106);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Latonia', 'Nokia 3110', 35);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Savannah', 'moto x4 Android One', 553);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Keneth', 'iPhone 5', 680);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Mose', 'HTC Bolt', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Keneth', 'HTC Bolt', 59);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Columbus', 'HTC U Ultra', 575);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Burl', 'Nokia', 58);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Jermaine', 'Samsung Note5', 444);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Asha', 'iPhone 5s', 526);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Kathleen', 'iPhone', 576);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Caryl', 'LG G6', 436);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Alexia', 'iPhone X', 127);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Cuc', 'Samsung S8+', 39);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Chang', 'HTC U11 life', 308);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Charlsie', 'Samsung A8', 492);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Leandro', 'iPhone 5', 1199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Jermaine', 'Samsung A8', 14);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Alexia', 'Samsung', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Josiah', 'Sony Xperia R1', 537);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Pasquale', 'Samsung Note5', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Delinda', 'iPhone 8', 327);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Jarrett', 'HTC Bolt', 318);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Savannah', 'Samsung S8+', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Holley', 'Nokia 6', 298);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Otis', 'moto x4', 206);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Keneth', 'moto e4 Amazon Prime', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Alexia', 'iPhone 8', 143);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Henry', 'Samsung', 219);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Freddy', 'Samsung Galaxy J5', 44);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Claud', 'HTC Bolt', 217);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Chang', 'Nokia 8', 106);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Doretta', 'HTC U Ultra', 37);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Lenny', 'Nokia 3110', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Linette', 'LG', 70);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Gwyneth', 'LG G6+', 47);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Pamila', 'Sony Xperia L1', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Tyrone', 'Sony Xperia R1', 332);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Josiah', 'iPhone', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Charlsie', 'moto x4', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Cole', 'iPhone', 442);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Tyrone', 'moto e4 Amazon Prime', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Avelina', 'iPhone 5s', 326);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Lenny', 'iPhone', 521);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Jess', 'iPhone 8', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Tyrone', 'iPhone', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Latonia', 'LG V30', 459);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Hung', 'HTC U Ultra', 599);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Jarrett', 'iPhone 6 Plus', 197);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Henry', 'Sony Xperia', 469);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Anthony', 'Sony Xperia XZ Premium', 11);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Shantel', 'LG V30+', 474);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Annita', 'iPhone 6 Plus', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Columbus', 'Samsung A8', 526);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Deloise', 'Nokia 3110', 248);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Andreas', 'Nokia', 591);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Katie', 'Samsung A8', 251);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Pasquale', 'Samsung', 483);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Jacques', 'Nokia', 341);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Lelah', 'iPhone', 42);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Cole', 'Nokia 8', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Henry', 'iPhone 8 Plus', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Cole', 'Nokia 3110', 366);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Columbus', 'Samsung Galaxy J5', 592);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Katie', 'LG', 324);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Edwardo', 'Nokia', 561);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Cuc', 'iPhone', 461);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Alexia', 'Samsung S8+', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Charlsie', 'Samsung', 362);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Tad', 'Nokia', 523);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Pamila', 'Samsung', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Roseline', 'Nokia 6', 398);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Jene', 'Samsung', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Jarrett', 'Nokia 3110', 391);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Alexia', 'Samsung A8', 515);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Pamila', 'moto x4 Android One', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Margaret', 'Sony Xperia', 363);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Esmeralda', 'LG', 191);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Savannah', 'LG', 139);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Gregorio', 'Nokia 3110', 369);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Mose', 'iPhone 6s', 511);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Katie', 'HTC U11', 277);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Kathaleen', 'Sony Xperia', 316);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Dana', 'iPhone 8', 227);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Tawna', 'Nokia 3110', 216);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Kathleen', 'Nokia 3110', 100);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Gemma', 'Nokia 8', 84);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Elinore', 'moto x4 Android One', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Phebe', 'LG G6+', 519);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Kathleen', 'iPhone 6s', 206);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Jacques', 'Nokia', 66);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Gwyneth', 'LG V30+', 410);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Tad', 'Samsung Note5', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Jermaine', 'Nokia 6', 1089);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Antonio', 'HTC U11', 488);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Cyril', 'HTC Bolt', 402);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Esmeralda', 'iPhone 5s', 258);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Ivory', 'Sony Xperia R1', 534);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Henry', 'Samsung Note5', 633);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Tad', 'Nokia 8', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Kandace', 'Samsung', 611);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Doretta', 'Samsung Galaxy J5', 457);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Keneth', 'Nokia 8', 449);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Tawna', 'iPhone 6s', 520);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Alexia', 'iPhone 6', 459);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Andreas', 'Sony Xperia', 139);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Pamila', 'Nokia 3110', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Anthony', 'Nokia', 502);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Phebe', 'Samsung Galaxy J5', 238);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Alexia', 'Nokia 8', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Annita', 'Sony Xperia R1', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Kathleen', 'HTC U Ultra', 253);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Chang', 'Samsung Note5', 571);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Dana', 'LG V30', 448);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Doretta', 'Samsung', 344);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Tyrone', 'Sony Xperia R1', 242);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Charlsie', 'Sony Xperia', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Katie', 'Nokia 8', 326);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Frederick', 'LG G6+', 153);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Ahmad', 'iPhone 6s', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Ivory', 'Nokia 6', 126);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Cole', 'moto e4 Amazon Prime', 584);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Hung', 'iPhone 6 Plus', 118);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Darius', 'Samsung S8+', 445);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Annita', 'HTC U Ultra', 444);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Pamila', 'Nokia 8', 25);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Jess', 'iPhone 8', 314);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Shantel', 'moto e4 Amazon Prime', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Doretta', 'Sony Xperia', 548);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Jene', 'Sony Xperia L1', 84);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Josiah', 'iPhone X', 229);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Jess', 'iPhone 6s', 283);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Antonio', 'iPhone 6 Plus', 569);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Elsa', 'Nokia 3110', 349);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Pasquale', 'LG G6+', 337);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Lenny', 'LG G6+', 121);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Frederick', 'HTC U11', 422);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Tyrone', 'iPhone', 156);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Mose', 'iPhone 5s', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Latonia', 'HTC U Ultra', 674);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Esmeralda', 'Samsung', 33);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Holley', 'iPhone', 567);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Phebe', 'Nokia 6', 374);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Delinda', 'iPhone 5s', 316);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Tawna', 'LG V30+', 633);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Darnell', 'Samsung J7 Pro', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Kathaleen', 'iPhone 6', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Claud', 'iPhone', 454);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Howard', 'Sony Xperia R1', 576);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Tamar', 'iPhone 8 Plus', 461);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Antonio', 'iPhone 5s', 55);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Darnell', 'HTC U11 life', 186);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Josiah', 'Sony Xperia L1', 493);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Claud', 'Samsung Note5', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Phebe', 'iPhone 6s', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Lenny', 'Samsung S8', 103);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Holley', 'iPhone 6s', 243);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Lenny', 'Sony Xperia L1', 31);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Kandace', 'Nokia 3110', 165);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Elsa', 'iPhone X', 380);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Jess', 'Nokia 3110', 61);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Fredericka', 'moto x4 Android One', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Chang', 'Nokia', 224);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Cuc', 'Sony Xperia R1', 533);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Kandace', 'HTC Bolt', 517);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Annita', 'Nokia 3110', 58);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Avelina', 'Sony Xperia L1', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Caryl', 'Samsung Note5', 529);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Josiah', 'iPhone', 1026);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Delinda', 'Nokia', 264);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Alison', 'HTC Bolt', 455);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Maryann', 'HTC U Ultra', 199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Sydney', 'HTC Bolt', 570);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Heather', 'Sony Xperia', 141);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Kathaleen', 'Samsung A8', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Columbus', 'Nokia 8', 518);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Antonio', 'Nokia 3110', 392);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Dana', 'Nokia 8', 214);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Jene', 'LG V30', 313);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Esmeralda', 'Samsung J7 Pro', 417);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Ahmad', 'moto x4 Android One', 525);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Keneth', 'iPhone', 544);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Caryl', 'iPhone 6', 746);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Shantel', 'LG', 353);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Jermaine', 'iPhone X', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Genie', 'Samsung S8+', 171);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Margaret', 'LG V30+', 98);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Delinda', 'iPhone 8 Plus', 23);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Fredericka', 'Samsung Note5', 251);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Keneth', 'Nokia 3110', 519);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Rocco', 'Sony Xperia R1', 88);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Gwyneth', 'Nokia', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Burl', 'moto x4', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Wyatt', 'Sony Xperia R1', 363);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Cole', 'HTC Bolt', 24);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Mose', 'LG V30+', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Hung', 'iPhone 5s', 172);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Delinda', 'Nokia', 371);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Margaret', 'HTC Bolt', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Jermaine', 'HTC U11', 222);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Tyrone', 'Samsung Galaxy J5', 280);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Holley', 'iPhone 6s', 64);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Gemma', 'Nokia 8', 219);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Tamar', 'iPhone X', 579);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Tawna', 'LG V30+', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Linette', 'iPhone X', 391);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Maude', 'Samsung', 409);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Fredericka', 'Samsung', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Teri', 'iPhone 6 Plus', 561);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Genie', 'HTC U11', 316);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Kathleen', 'Nokia 8', 293);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Don', 'iPhone 6s', 864);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Tad', 'iPhone X', 26);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Deloise', 'Nokia', 308);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Hung', 'LG V30', 581);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Anthony', 'Samsung Galaxy J5', 104);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Jene', 'Samsung Note5', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Teri', 'Nokia 3110', 379);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Teri', 'Samsung A8', 190);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Frederick', 'Nokia 3110', 118);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Mose', 'Samsung', 83);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Maude', 'Nokia 6', 175);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Asha', 'Samsung', 54);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Dale', 'LG V30+', 111);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Holley', 'iPhone X', 122);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Linette', 'Sony Xperia', 365);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Kathaleen', 'Nokia 6', 455);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Antoine', 'HTC U Ultra', 312);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Kandace', 'Samsung', 455);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Caryl', 'Nokia 3110', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Holley', 'Nokia 3110', 236);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Jacques', 'iPhone X', 324);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Chang', 'HTC U11 life', 270);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Don', 'Samsung', 95);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Chang', 'Sony Xperia R1', 517);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Antoine', 'moto x4 Android One', 428);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Jarrett', 'HTC U11', 110);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Elsa', 'moto x4', 834);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Antonio', 'LG', 72);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Wyatt', 'iPhone 8 Plus', 286);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Frederick', 'iPhone 5', 10);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Sydney', 'LG', 201);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Dana', 'Samsung Note5', 112);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Linette', 'Samsung J7 Pro', 533);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Jermaine', 'HTC U11 life', 492);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Rocco', 'Nokia 6', 364);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Asha', 'Samsung J7 Pro', 171);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Hung', 'Nokia 6', 116);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Caryl', 'LG V30', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Frederick', 'Nokia', 569);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Maryann', 'moto e4 Amazon Prime', 332);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Freddy', 'HTC U Ultra', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Pamila', 'iPhone 6s', 440);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Anthony', 'Nokia 3110', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Annita', 'Nokia', 311);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Rocco', 'Samsung', 186);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Shantel', 'HTC Bolt', 205);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Elinore', 'moto e4 Amazon Prime', 79);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Pasquale', 'iPhone 6', 814);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Genie', 'Samsung Galaxy J5', 60);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Mignon', 'LG G6', 262);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Avelina', 'iPhone 6s', 211);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Cyril', 'moto x4', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Tamar', 'moto x4', 480);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Henry', 'LG V30+', 446);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Kandace', 'HTC U Ultra', 51);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Kathleen', 'LG G6+', 367);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Jess', 'Samsung A8', 181);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Kathaleen', 'LG', 1086);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Asha', 'Samsung Note5', 107);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Ivory', 'Samsung S8+', 179);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Kathleen', 'iPhone', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Shantel', 'LG', 54);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Henry', 'Samsung', 315);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Katie', 'moto e4 Amazon Prime', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Tawna', 'iPhone', 376);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Cole', 'LG G6+', 1024);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Cole', 'Samsung', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Dale', 'Samsung Note5', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Columbus', 'iPhone 8', 434);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Roseline', 'Nokia 8', 1161);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Deloise', 'HTC Bolt', 81);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Frederick', 'Nokia 3110', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Tawna', 'Samsung S8', 339);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Wyatt', 'Samsung S8+', 386);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Josiah', 'Samsung Note5', 246);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Asha', 'Samsung Note5', 339);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Darnell', 'iPhone 5s', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Anthony', 'LG V30+', 33);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Cole', 'LG V30', 70);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Alexia', 'iPhone 6s', 175);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Shantel', 'iPhone 5s', 99);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Avelina', 'Samsung J7 Pro', 355);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Esmeralda', 'iPhone 8', 538);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Fredericka', 'iPhone X', 1016);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Howard', 'LG', 120);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Don', 'HTC Bolt', 598);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Dana', 'Nokia 8', 950);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Andre', 'HTC U Ultra', 492);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Phebe', 'LG', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Ivory', 'Nokia', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Roseline', 'Samsung', 373);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Hung', 'moto e4 Amazon Prime', 493);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Mose', 'iPhone', 545);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Tamar', 'Nokia', 517);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Heather', 'iPhone', 150);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Maryann', 'Nokia 3110', 353);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Jess', 'LG G6+', 288);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Fredericka', 'HTC Bolt', 369);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Tamar', 'Samsung A8', 851);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Cyril', 'iPhone', 522);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Gwyneth', 'HTC U11', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Howard', 'Sony Xperia XZ Premium', 488);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Kathleen', 'iPhone 5', 76);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Antoine', 'LG V30+', 38);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Margaret', 'Nokia 3110', 312);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Alexia', 'iPhone X', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Savannah', 'HTC U11', 40);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Phebe', 'Sony Xperia L1', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Maude', 'Samsung A8', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Claud', 'HTC Bolt', 417);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Dale', 'Samsung S8+', 39);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Dale', 'iPhone X', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Andre', 'Sony Xperia', 399);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Cole', 'LG V30+', 352);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Keneth', 'iPhone 6 Plus', 295);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Kathaleen', 'iPhone X', 910);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Mose', 'Nokia 3110', 116);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Henry', 'Samsung Note5', 40);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Burl', 'iPhone', 270);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Freddy', 'iPhone X', 357);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Hung', 'iPhone 5s', 378);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Otis', 'Samsung J7 Pro', 276);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Esmeralda', 'iPhone 5s', 459);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Teri', 'moto x4 Android One', 23);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Deloise', 'iPhone 6 Plus', 154);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Don', 'Sony Xperia R1', 213);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Tawna', 'Nokia 6', 277);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Cyril', 'Nokia', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Elinore', 'iPhone 8', 503);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Charlsie', 'iPhone', 377);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Anthony', 'LG V30+', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Chang', 'iPhone 5s', 55);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Latonia', 'LG V30+', 518);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Dana', 'LG', 24);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Jacques', 'HTC U Ultra', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Alexia', 'Nokia 6', 63);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Caryl', 'Nokia 3110', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Kathaleen', 'moto e4 Amazon Prime', 577);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Andre', 'Nokia 6', 569);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Caryl', 'LG V30+', 468);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Otis', 'HTC U11', 229);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Dana', 'moto x4 Android One', 547);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Alison', 'iPhone', 523);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Teri', 'LG V30', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Phebe', 'iPhone 8 Plus', 270);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Gwyneth', 'Nokia', 252);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Alison', 'Sony Xperia R1', 209);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Jene', 'iPhone', 837);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Pamila', 'Nokia 3110', 291);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Claud', 'Samsung', 16);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Roseline', 'HTC U11 life', 82);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Teri', 'moto e4 Amazon Prime', 63);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Charlsie', 'iPhone 8 Plus', 475);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Frederick', 'Samsung A8', 199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Grant', 'Nokia 8', 467);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Wyatt', 'HTC Bolt', 31);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Cuc', 'Sony Xperia', 480);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Darius', 'Samsung J7 Pro', 491);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Howard', 'iPhone 6', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Gregorio', 'LG', 343);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Dana', 'iPhone 5', 598);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Anthony', 'LG', 372);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Avelina', 'Nokia 8', 171);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Kandace', 'Samsung S8', 474);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Dale', 'iPhone 6 Plus', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Mignon', 'Samsung', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Otis', 'LG V30', 358);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Maryann', 'Nokia 3110', 46);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Claud', 'Sony Xperia XZ Premium', 578);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Jacques', 'iPhone 5s', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Antoine', 'Sony Xperia L1', 272);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Grant', 'Sony Xperia XZ Premium', 565);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Linette', 'Nokia 3110', 110);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Tamar', 'Samsung Galaxy J5', 82);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Holley', 'iPhone 8 Plus', 88);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Freddy', 'Samsung S8+', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Elinore', 'Samsung A8', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Lenny', 'LG V30', 135);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Gwyneth', 'iPhone 6', 193);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Heather', 'iPhone 6', 237);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Kathleen', 'iPhone 6s', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Katie', 'Samsung S8+', 390);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Gemma', 'LG G6', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Kandace', 'Sony Xperia', 79);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Julianna', 'moto e4 Amazon Prime', 813);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Elinore', 'Nokia 3110', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Jermaine', 'HTC Bolt', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Phebe', 'LG V30+', 532);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Leandro', 'Samsung', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Tyrone', 'iPhone', 15);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Mignon', 'HTC U11', 515);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Anthony', 'HTC U11 life', 552);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Asha', 'Samsung', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Dana', 'iPhone X', 182);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Henry', 'Nokia', 547);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Gwyneth', 'iPhone 6', 113);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Kathleen', 'LG G6+', 586);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Chang', 'iPhone 5s', 598);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Otis', 'Samsung S8+', 63);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Lelah', 'Sony Xperia', 250);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Frederick', 'Nokia', 182);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Deloise', 'iPhone X', 239);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Esmeralda', 'Sony Xperia R1', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Keneth', 'HTC Bolt', 181);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Leandro', 'iPhone 6s', 207);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Alexia', 'Nokia', 448);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Burl', 'HTC U11', 25);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Cuc', 'Samsung S8', 529);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Deloise', 'LG V30', 314);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Ahmad', 'Samsung Note5', 289);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Anthony', 'HTC Bolt', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Doretta', 'iPhone X', 507);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Alison', 'Nokia 3110', 315);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Pasquale', 'moto e4 Amazon Prime', 58);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Maude', 'Nokia', 814);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Phebe', 'HTC Bolt', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Leana', 'Sony Xperia R1', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Wyatt', 'HTC Bolt', 410);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Cole', 'iPhone', 192);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Anthony', 'LG G6', 552);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Frederick', 'Nokia 3110', 212);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Heather', 'moto e4 Amazon Prime', 173);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Latonia', 'iPhone', 463);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Grant', 'iPhone 6s', 250);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Linette', 'Nokia 6', 790);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Sydney', 'iPhone 6s', 169);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Phebe', 'iPhone 8', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Kathaleen', 'LG G6+', 544);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Darius', 'iPhone X', 472);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Leandro', 'LG G6', 37);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Esmeralda', 'HTC U Ultra', 160);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Mose', 'Nokia 3110', 286);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Elsa', 'HTC U11', 589);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Kandace', 'iPhone', 495);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Frederick', 'LG G6+', 1000);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Caryl', 'LG V30+', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Lenny', 'Samsung Galaxy J5', 287);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Annita', 'LG', 138);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Tyrone', 'Samsung Galaxy J5', 394);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Teri', 'iPhone', 229);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Claud', 'LG V30+', 212);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Asha', 'LG V30+', 675);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Linette', 'iPhone', 356);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Lenny', 'Nokia 3110', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Latonia', 'Samsung S8+', 186);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Anthony', 'moto e4 Amazon Prime', 364);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Charlsie', 'LG V30', 363);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Charlsie', 'iPhone X', 415);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Cuc', 'LG', 330);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Andreas', 'Samsung J7 Pro', 384);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Annita', 'HTC Bolt', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Don', 'Sony Xperia L1', 385);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Annita', 'Samsung A8', 232);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Rocco', 'Nokia 6', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Roseline', 'Nokia 3110', 344);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Chang', 'HTC U11 life', 331);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Otis', 'Samsung Note5', 47);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Delinda', 'HTC U Ultra', 575);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Gemma', 'LG G6+', 471);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Jacques', 'moto e4 Amazon Prime', 1024);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Darnell', 'Samsung Note5', 62);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Savannah', 'Nokia 8', 145);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Henry', 'Nokia 6', 1126);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Mose', 'LG V30', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Roseline', 'Samsung A8', 96);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Darius', 'HTC U11 life', 484);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Pasquale', 'moto x4 Android One', 39);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Keneth', 'LG', 528);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Chang', 'Nokia 6', 421);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Cuc', 'Samsung Galaxy J5', 121);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Phebe', 'iPhone 5', 851);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Fredericka', 'moto e4 Amazon Prime', 651);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Rocco', 'Sony Xperia L1', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Howard', 'LG V30', 440);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Maryann', 'Sony Xperia', 129);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Henry', 'moto x4 Android One', 357);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Roseline', 'LG V30', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Frederick', 'Samsung', 421);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Anthony', 'HTC U11 life', 91);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Elsa', 'LG G6+', 132);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Phebe', 'Samsung S8', 998);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Pasquale', 'Samsung J7 Pro', 378);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Otis', 'iPhone X', 278);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Shantel', 'Samsung', 94);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Dana', 'Nokia 3110', 1162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Dale', 'HTC U Ultra', 371);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Savannah', 'LG', 178);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Katie', 'iPhone X', 187);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Jess', 'HTC U11 life', 460);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Hung', 'iPhone 6s', 285);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Fredericka', 'Samsung Galaxy J5', 516);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Cyril', 'moto e4 Amazon Prime', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Gemma', 'Nokia 8', 207);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Darius', 'HTC U Ultra', 411);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Teri', 'iPhone 6s', 552);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Leandro', 'Sony Xperia XZ Premium', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Dale', 'iPhone 8', 301);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Kathleen', 'Nokia 3110', 90);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Gregorio', 'Samsung J7 Pro', 99);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Esmeralda', 'HTC Bolt', 509);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Burl', 'iPhone', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Columbus', 'Samsung Note5', 101);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Keneth', 'Samsung Note5', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Alison', 'Sony Xperia L1', 565);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Lelah', 'iPhone', 206);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Esmeralda', 'Samsung Note5', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Antoine', 'moto x4 Android One', 346);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Jarrett', 'LG', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Pamila', 'iPhone', 229);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Darius', 'Samsung', 119);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Asha', 'HTC U Ultra', 542);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Elsa', 'Sony Xperia R1', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Burl', 'LG G6+', 411);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Jacques', 'iPhone 8', 257);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Tawna', 'Samsung Note5', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Maude', 'iPhone 6s', 492);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Jacques', 'Samsung', 596);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Katie', 'moto x4', 367);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Roseline', 'iPhone 8', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Otis', 'iPhone 8', 38);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Ivory', 'Samsung Galaxy J5', 162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Holley', 'Samsung', 471);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Roseline', 'moto e4 Amazon Prime', 1051);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Asha', 'Sony Xperia XZ Premium', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Linette', 'HTC U11', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Gregorio', 'LG V30', 201);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Jarrett', 'HTC U Ultra', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Alexia', 'Samsung', 39);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Shantel', 'LG G6+', 85);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Keneth', 'Samsung Note5', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Gregorio', 'iPhone X', 111);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Shantel', 'moto x4 Android One', 264);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Mose', 'iPhone 6s', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Ahmad', 'HTC Bolt', 40);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Dale', 'Samsung A8', 345);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Jarrett', 'HTC U Ultra', 412);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Hung', 'iPhone 5', 113);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Columbus', 'iPhone', 500);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Savannah', 'Samsung Galaxy J5', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Wyatt', 'Samsung S8+', 591);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Roseline', 'Samsung', 181);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Kathleen', 'Samsung A8', 139);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Cyril', 'iPhone X', 566);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Esmeralda', 'iPhone 6s', 298);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Jermaine', 'Samsung S8', 483);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Chang', 'LG V30+', 234);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Howard', 'HTC U11', 495);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Savannah', 'Nokia 8', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Freddy', 'iPhone 5', 399);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Heather', 'iPhone 6 Plus', 217);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Don', 'HTC Bolt', 737);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Freddy', 'Samsung', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Savannah', 'iPhone 6', 349);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Don', 'LG', 453);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Latonia', 'Nokia 6', 542);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Latonia', 'iPhone 5', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Darnell', 'iPhone 8 Plus', 351);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Leandro', 'Samsung J7 Pro', 251);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Edwardo', 'Nokia 8', 404);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Dana', 'HTC U11', 172);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Sydney', 'LG G6', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Cyril', 'Sony Xperia R1', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Keneth', 'LG G6', 560);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Wyatt', 'Nokia', 14);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Cole', 'Samsung Note5', 188);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Gwyneth', 'iPhone', 175);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Howard', 'Nokia 3110', 487);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Kathaleen', 'moto e4 Amazon Prime', 260);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Maryann', 'Sony Xperia XZ Premium', 513);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Savannah', 'Nokia 3110', 144);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Darius', 'Nokia 8', 331);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Sydney', 'Sony Xperia', 522);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Charlsie', 'Nokia', 279);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Burl', 'iPhone', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Frederick', 'LG G6', 150);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Mignon', 'iPhone 6s', 561);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Charlsie', 'Sony Xperia', 518);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Cyril', 'moto e4 Amazon Prime', 471);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Teri', 'iPhone', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Burl', 'LG V30', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Jarrett', 'HTC U11 life', 208);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Frederick', 'Samsung Galaxy J5', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Katie', 'iPhone 8 Plus', 493);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Shantel', 'Samsung Galaxy J5', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Grant', 'iPhone 5s', 168);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Keneth', 'Samsung', 143);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Gregorio', 'Nokia', 559);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Latonia', 'iPhone', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Darnell', 'iPhone 6s', 195);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Delinda', 'HTC U Ultra', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Linette', 'Sony Xperia', 168);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Chang', 'HTC U Ultra', 216);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Kathaleen', 'LG V30', 155);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Frederick', 'iPhone 5s', 84);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Jacques', 'iPhone 6', 173);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Leana', 'iPhone X', 75);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Edwardo', 'moto e4 Amazon Prime', 152);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Keneth', 'iPhone 8', 256);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Otis', 'iPhone 6s', 163);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Fredericka', 'iPhone 5', 205);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Gemma', 'moto x4 Android One', 292);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Shantel', 'LG', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Ivory', 'Nokia 3110', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Claud', 'moto e4 Amazon Prime', 40);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Gregorio', 'iPhone', 336);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Jermaine', 'iPhone 8', 312);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Ivory', 'Sony Xperia', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Gemma', 'iPhone 6s', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Kathleen', 'Samsung Note5', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Dale', 'Nokia 6', 205);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Teri', 'iPhone', 546);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Dale', 'iPhone 6 Plus', 279);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Shantel', 'iPhone', 325);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Leandro', 'Samsung A8', 428);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Kandace', 'Samsung', 128);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Don', 'iPhone 6s', 993);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Margaret', 'LG', 1196);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Ahmad', 'Sony Xperia', 545);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Maryann', 'iPhone 5', 23);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Kathleen', 'moto e4 Amazon Prime', 534);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Lelah', 'iPhone 8 Plus', 227);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Roseline', 'Samsung J7 Pro', 172);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Jarrett', 'Nokia 3110', 346);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Mose', 'Samsung Galaxy J5', 406);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Julianna', 'HTC U11 life', 68);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Jacques', 'Nokia', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Leana', 'moto x4 Android One', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Rocco', 'iPhone 6', 116);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Katie', 'Samsung J7 Pro', 851);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Dale', 'Nokia 3110', 52);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Annita', 'Samsung Note5', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Roseline', 'moto x4', 100);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Elinore', 'Samsung A8', 349);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Ahmad', 'iPhone 8 Plus', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Maude', 'moto x4 Android One', 396);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Delinda', 'LG V30', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Andreas', 'Samsung S8+', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Josiah', 'Samsung S8+', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Lelah', 'LG', 72);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Jene', 'HTC Bolt', 160);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Mignon', 'Sony Xperia', 260);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Alison', 'Nokia 6', 529);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Katie', 'HTC U11', 215);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Roseline', 'iPhone 5s', 135);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Latonia', 'moto x4', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Doretta', 'iPhone 6', 134);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Annita', 'Nokia', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Heather', 'Nokia 6', 336);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Anthony', 'iPhone', 360);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Frederick', 'moto e4 Amazon Prime', 44);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Tawna', 'Samsung A8', 499);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Ivory', 'Samsung Note5', 192);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Josiah', 'iPhone X', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Darnell', 'LG V30', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Heather', 'LG G6', 302);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Annita', 'moto x4 Android One', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Darius', 'HTC U11 life', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Tawna', 'Nokia 3110', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Avelina', 'Nokia 6', 192);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Phebe', 'iPhone', 42);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Kandace', 'Samsung S8', 151);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Deloise', 'iPhone 6 Plus', 83);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Savannah', 'HTC U Ultra', 245);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Henry', 'Samsung A8', 163);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Caryl', 'HTC U Ultra', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Elsa', 'LG G6+', 334);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Kathleen', 'Samsung Note5', 236);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Henry', 'Nokia 8', 704);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Asha', 'iPhone 6s', 206);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Annita', 'iPhone 5s', 445);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Teri', 'Nokia 6', 261);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Howard', 'iPhone 8 Plus', 534);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Cole', 'LG V30', 494);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Asha', 'Nokia', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Ahmad', 'Nokia 8', 322);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Elsa', 'LG G6+', 421);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Pasquale', 'HTC U Ultra', 45);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Heather', 'Sony Xperia', 24);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Kandace', 'Samsung Note5', 1183);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Freddy', 'moto x4 Android One', 287);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Katie', 'iPhone 5', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Claud', 'LG G6+', 469);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Pamila', 'iPhone 6', 61);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Elinore', 'LG G6', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Andre', 'Nokia 8', 188);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Fredericka', 'Samsung', 82);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Darnell', 'Samsung J7 Pro', 193);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Pamila', 'Nokia 6', 162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Asha', 'Sony Xperia R1', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Cole', 'iPhone 6', 439);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Caryl', 'Samsung', 343);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Don', 'moto x4 Android One', 845);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Don', 'Samsung J7 Pro', 377);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Fredericka', 'iPhone 5', 269);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Tamar', 'LG V30+', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Teri', 'Nokia 3110', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Tyrone', 'moto e4 Amazon Prime', 288);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Tyrone', 'Samsung', 264);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Antonio', 'iPhone 6s', 82);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Gemma', 'LG', 353);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Wyatt', 'iPhone 6 Plus', 652);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Esmeralda', 'LG G6+', 234);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Jacques', 'iPhone 6 Plus', 234);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Tawna', 'LG G6', 1030);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Don', 'Nokia 3110', 402);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Antoine', 'Samsung Note5', 259);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Anthony', 'HTC Bolt', 559);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Maude', 'LG', 71);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Freddy', 'iPhone 5s', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Charlsie', 'iPhone 5s', 383);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Gemma', 'Nokia 3110', 518);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Gregorio', 'moto x4 Android One', 549);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Jermaine', 'HTC U11', 32);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Latonia', 'Samsung', 432);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Kathleen', 'Samsung Note5', 478);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Leana', 'Nokia 3110', 439);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Howard', 'Sony Xperia XZ Premium', 90);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Jacques', 'Samsung', 121);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Fredericka', 'Samsung', 123);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Gemma', 'LG G6+', 527);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Caryl', 'Sony Xperia R1', 197);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Howard', 'HTC Bolt', 77);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Gemma', 'iPhone 6s', 237);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Tamar', 'HTC Bolt', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Frederick', 'HTC Bolt', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Teri', 'iPhone X', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Howard', 'HTC Bolt', 539);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Maryann', 'HTC Bolt', 571);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Rocco', 'HTC U11', 179);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Cole', 'Samsung', 592);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Darnell', 'HTC U11', 396);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Delinda', 'iPhone X', 509);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Avelina', 'Nokia 8', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Doretta', 'iPhone 5', 354);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Caryl', 'HTC U Ultra', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Kandace', 'Nokia 3110', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Cuc', 'HTC Bolt', 118);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Esmeralda', 'Samsung', 147);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Pasquale', 'iPhone 6', 639);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Elsa', 'Samsung A8', 37);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Caryl', 'Sony Xperia L1', 12);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Fredericka', 'moto x4 Android One', 58);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Cole', 'Samsung', 527);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Keneth', 'HTC Bolt', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Leandro', 'iPhone', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Gregorio', 'moto e4 Amazon Prime', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Leana', 'moto e4 Amazon Prime', 595);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Jacques', 'Samsung Note5', 517);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Jarrett', 'iPhone 5s', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Otis', 'LG', 586);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Rocco', 'moto x4 Android One', 293);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Shantel', 'Samsung Galaxy J5', 198);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Pamila', 'Sony Xperia', 361);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Dale', 'Sony Xperia', 60);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Alexia', 'Nokia 6', 231);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Phebe', 'iPhone', 448);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Holley', 'Sony Xperia L1', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Annita', 'LG', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Howard', 'Sony Xperia XZ Premium', 485);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Maude', 'moto x4 Android One', 226);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Darius', 'Nokia 3110', 54);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Annita', 'iPhone 6 Plus', 430);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Elinore', 'HTC U11', 212);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Andreas', 'Sony Xperia R1', 341);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Josiah', 'Sony Xperia XZ Premium', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Charlsie', 'Samsung Galaxy J5', 874);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Fredericka', 'Nokia 3110', 434);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Charlsie', 'iPhone 5', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Latonia', 'iPhone', 343);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Alison', 'HTC Bolt', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Teri', 'Samsung', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Kathaleen', 'LG G6', 166);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Cuc', 'Nokia 3110', 526);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Cyril', 'Samsung A8', 416);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Tawna', 'iPhone', 124);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Tamar', 'iPhone', 330);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Katie', 'Samsung', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Shantel', 'LG G6+', 491);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Wyatt', 'iPhone 6', 548);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Wyatt', 'Samsung S8+', 378);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Tawna', 'Nokia', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Andre', 'Nokia', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Esmeralda', 'Sony Xperia', 131);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Linette', 'Samsung Galaxy J5', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Wyatt', 'Sony Xperia R1', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Delinda', 'moto x4', 285);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Josiah', 'iPhone X', 24);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Esmeralda', 'LG G6', 155);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Don', 'iPhone X', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Phebe', 'Sony Xperia XZ Premium', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Tad', 'HTC Bolt', 150);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Fredericka', 'moto e4 Amazon Prime', 172);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Dale', 'HTC Bolt', 528);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Sydney', 'Samsung J7 Pro', 549);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Jacques', 'Sony Xperia R1', 121);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Henry', 'Samsung', 261);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Mose', 'Samsung Galaxy J5', 85);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Pasquale', 'iPhone 8', 25);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Mose', 'Sony Xperia XZ Premium', 128);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Cyril', 'Sony Xperia XZ Premium', 238);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Fredericka', 'Samsung J7 Pro', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Dana', 'moto x4 Android One', 141);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Darnell', 'Nokia 3110', 60);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Fredericka', 'HTC U11', 87);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Jess', 'iPhone 6s', 326);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Fredericka', 'Sony Xperia XZ Premium', 1132);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Margaret', 'Sony Xperia R1', 162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Leana', 'iPhone', 201);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Julianna', 'Samsung Note5', 873);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Howard', 'Nokia 8', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Phebe', 'LG G6+', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Andreas', 'LG', 991);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Don', 'Samsung', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Lenny', 'Nokia 6', 214);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Tad', 'Samsung S8', 302);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Deloise', 'Samsung Galaxy J5', 507);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Teri', 'Samsung Galaxy J5', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Antonio', 'iPhone 8', 111);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Claud', 'Samsung S8+', 304);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Elinore', 'Samsung Galaxy J5', 235);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Antoine', 'Nokia 6', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Kathaleen', 'Samsung S8', 363);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Doretta', 'iPhone 5', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Katie', 'Samsung Note5', 248);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Lelah', 'iPhone 8', 384);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Wyatt', 'LG V30', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Margaret', 'Samsung Note5', 161);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Caryl', 'Samsung J7 Pro', 482);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Jacques', 'HTC U11', 341);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Edwardo', 'moto x4 Android One', 453);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Maryann', 'LG G6+', 568);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Charlsie', 'Samsung Note5', 435);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Maryann', 'Nokia', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Maude', 'iPhone 6 Plus', 521);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Jene', 'Nokia 6', 466);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Tamar', 'iPhone 6 Plus', 224);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Katie', 'iPhone 6 Plus', 528);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Henry', 'Samsung', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Tawna', 'HTC Bolt', 585);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Dale', 'Nokia 8', 431);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Cyril', 'Samsung A8', 568);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Tamar', 'iPhone X', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Pasquale', 'Nokia 3110', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Jarrett', 'moto e4 Amazon Prime', 305);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Burl', 'Nokia 8', 287);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Pasquale', 'LG G6+', 255);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Doretta', 'HTC U11', 516);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Freddy', 'LG V30', 590);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Antonio', 'Samsung A8', 227);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Columbus', 'iPhone 6 Plus', 264);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Ivory', 'HTC Bolt', 430);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Chang', 'Nokia 3110', 288);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Kathaleen', 'HTC Bolt', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Leana', 'Nokia 3110', 576);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Charlsie', 'Samsung Galaxy J5', 147);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Deloise', 'Samsung J7 Pro', 389);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Dana', 'iPhone', 73);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Alexia', 'Nokia 3110', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Josiah', 'Samsung J7 Pro', 327);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Dana', 'Nokia 8', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Elsa', 'HTC Bolt', 123);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Deloise', 'Samsung S8+', 223);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Teri', 'moto e4 Amazon Prime', 462);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Julianna', 'HTC Bolt', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Kathleen', 'Nokia 3110', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Cyril', 'iPhone', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Cuc', 'iPhone X', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Gemma', 'Samsung Galaxy J5', 95);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Elinore', 'Nokia 3110', 181);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Mose', 'Samsung', 275);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Jacques', 'iPhone 8 Plus', 654);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Annita', 'Nokia 3110', 377);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Grant', 'Samsung J7 Pro', 1119);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Shantel', 'Samsung J7 Pro', 11);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Darius', 'Nokia 3110', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Maryann', 'HTC Bolt', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Fredericka', 'Sony Xperia L1', 314);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Latonia', 'iPhone 5', 101);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Burl', 'moto x4', 286);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Cole', 'LG', 487);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Wyatt', 'Nokia', 224);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Gwyneth', 'LG V30+', 563);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Cyril', 'Samsung A8', 74);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Claud', 'Sony Xperia L1', 348);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Tawna', 'Samsung Galaxy J5', 1061);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Doretta', 'LG', 138);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Teri', 'LG V30+', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Chang', 'LG V30', 215);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Sydney', 'Nokia 6', 239);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Delinda', 'iPhone 8', 338);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Tawna', 'iPhone 6s', 347);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Margaret', 'moto x4 Android One', 550);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Mose', 'Samsung', 187);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Genie', 'Nokia 8', 643);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Anthony', 'Nokia 3110', 216);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Jene', 'moto x4 Android One', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Gregorio', 'LG G6', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Kandace', 'HTC U11', 1173);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Lelah', 'HTC U11 life', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Fredericka', 'Samsung', 124);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Shantel', 'iPhone X', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Anthony', 'HTC U Ultra', 543);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Fredericka', 'moto x4 Android One', 200);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Leandro', 'iPhone X', 198);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Alison', 'LG', 597);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Doretta', 'LG V30', 390);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Jene', 'Nokia 8', 138);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Jene', 'Nokia 3110', 299);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Jermaine', 'Nokia 8', 524);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Holley', 'Sony Xperia R1', 586);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Dana', 'Samsung A8', 600);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Dale', 'Samsung Note5', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Asha', 'LG V30', 450);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Mignon', 'Nokia 3110', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Ivory', 'moto x4 Android One', 498);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Fredericka', 'Nokia 3110', 494);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Tyrone', 'Samsung', 104);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Doretta', 'Nokia', 487);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Esmeralda', 'Sony Xperia L1', 275);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Dale', 'Samsung Note5', 141);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Chang', 'Nokia 3110', 449);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Kathleen', 'iPhone', 255);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Margaret', 'HTC U11 life', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Henry', 'Nokia 8', 256);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Alison', 'Sony Xperia L1', 579);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Mignon', 'moto e4 Amazon Prime', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Lelah', 'iPhone 6s', 525);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Fredericka', 'Sony Xperia R1', 563);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Darnell', 'Nokia', 495);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Sydney', 'moto e4 Amazon Prime', 413);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Holley', 'iPhone', 464);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Wyatt', 'iPhone 6 Plus', 200);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Leana', 'LG', 142);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Jarrett', 'Sony Xperia XZ Premium', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Keneth', 'Nokia 6', 1042);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Edwardo', 'Samsung Note5', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Jarrett', 'LG G6+', 75);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Alexia', 'Samsung S8+', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Freddy', 'HTC U11 life', 410);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Avelina', 'LG V30', 311);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Mose', 'Samsung Note5', 280);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Ivory', 'iPhone X', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Tawna', 'HTC U11', 1195);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Andreas', 'Samsung Galaxy J5', 334);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Esmeralda', 'moto e4 Amazon Prime', 507);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Sydney', 'iPhone', 287);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Andre', 'Nokia 6', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Jermaine', 'Samsung Note5', 42);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Maryann', 'iPhone X', 368);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Tyrone', 'HTC Bolt', 221);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Linette', 'Samsung S8+', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Otis', 'iPhone 5s', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Hung', 'iPhone 8', 252);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Andreas', 'iPhone 6s', 212);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Maude', 'LG V30+', 567);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Maude', 'Sony Xperia', 462);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Julianna', 'moto x4', 592);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Deloise', 'HTC Bolt', 479);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Fredericka', 'LG V30+', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Tad', 'Sony Xperia L1', 554);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Maryann', 'iPhone 5', 177);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Anthony', 'LG', 85);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Roseline', 'moto x4', 170);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Leana', 'iPhone 5s', 111);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Burl', 'iPhone 8', 492);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Keneth', 'iPhone 6', 117);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Ahmad', 'moto x4', 370);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Lenny', 'LG G6', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Margaret', 'iPhone X', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Jene', 'Samsung Note5', 151);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Burl', 'Nokia', 31);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Mignon', 'HTC Bolt', 30);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Jess', 'iPhone', 53);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Savannah', 'HTC Bolt', 424);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Wyatt', 'HTC Bolt', 569);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Mignon', 'Samsung A8', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Genie', 'Sony Xperia L1', 342);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Fredericka', 'Nokia', 315);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Phebe', 'HTC U Ultra', 74);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Tamar', 'iPhone 6s', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Elinore', 'Sony Xperia R1', 115);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Antoine', 'Sony Xperia XZ Premium', 246);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Maryann', 'moto x4 Android One', 100);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Darius', 'Nokia 6', 797);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Delinda', 'LG G6', 387);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Maryann', 'Nokia 6', 484);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Cuc', 'iPhone X', 267);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Doretta', 'Nokia 3110', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Delinda', 'Nokia 8', 486);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Leana', 'Samsung Note5', 534);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Edwardo', 'Nokia 3110', 505);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Tawna', 'HTC U Ultra', 581);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Dana', 'Samsung', 326);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Rocco', 'iPhone 6s', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Margaret', 'Samsung A8', 11);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Mignon', 'Sony Xperia', 600);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Ivory', 'Samsung S8+', 967);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Dana', 'iPhone 6s', 261);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Edwardo', 'Samsung', 38);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Jacques', 'LG', 391);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Leandro', 'iPhone 5s', 397);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Fredericka', 'LG', 299);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Doretta', 'Nokia 3110', 285);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Kathaleen', 'LG G6', 156);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Charlsie', 'Sony Xperia R1', 499);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Ahmad', 'HTC U11', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Doretta', 'Nokia 8', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Doretta', 'LG V30', 103);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Elsa', 'Samsung', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Fredericka', 'Samsung', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Frederick', 'moto x4 Android One', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Phebe', 'HTC U Ultra', 26);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Mignon', 'moto x4 Android One', 721);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Leana', 'LG V30+', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Ivory', 'Nokia 8', 310);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Claud', 'Nokia', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Alison', 'LG G6', 481);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Burl', 'Nokia 8', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Cole', 'LG V30', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Ahmad', 'Samsung Note5', 96);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Doretta', 'Samsung S8', 463);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Charlsie', 'LG', 258);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Edwardo', 'Nokia 3110', 59);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Julianna', 'iPhone 6', 468);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Anthony', 'Samsung Galaxy J5', 451);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Cuc', 'Samsung S8+', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Mignon', 'Nokia 6', 147);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Fredericka', 'Samsung Note5', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Chang', 'Samsung', 430);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Elsa', 'Nokia 3110', 310);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Keneth', 'LG V30', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Fredericka', 'Sony Xperia R1', 120);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Wyatt', 'iPhone 6s', 410);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Jermaine', 'iPhone', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Kathaleen', 'Nokia 3110', 586);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Jarrett', 'LG G6+', 844);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Delinda', 'iPhone 5', 219);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Maude', 'Samsung A8', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Teri', 'Sony Xperia', 592);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Jarrett', 'HTC Bolt', 267);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Esmeralda', 'HTC Bolt', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Roseline', 'LG', 35);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Savannah', 'Samsung', 116);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Dale', 'iPhone X', 226);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Burl', 'iPhone 5s', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Caryl', 'LG V30+', 82);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Ahmad', 'Nokia 8', 370);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Maude', 'iPhone 5s', 533);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Mose', 'iPhone 5s', 115);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Rocco', 'Samsung', 535);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Ivory', 'Nokia 3110', 211);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Jacques', 'iPhone 8 Plus', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Deloise', 'LG G6', 169);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Kathaleen', 'LG', 120);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Doretta', 'iPhone X', 386);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Holley', 'iPhone 6 Plus', 595);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Ahmad', 'Nokia 3110', 262);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Elinore', 'Samsung Note5', 533);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Tamar', 'LG G6', 15);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Alexia', 'Nokia 3110', 143);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Julianna', 'Nokia 3110', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Chang', 'Sony Xperia XZ Premium', 16);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Antoine', 'iPhone', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Ivory', 'iPhone 6s', 114);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Tawna', 'iPhone 5', 142);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Asha', 'Nokia 3110', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Linette', 'Samsung Note5', 481);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Hung', 'LG', 233);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Keneth', 'iPhone 6 Plus', 498);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Andre', 'Samsung A8', 87);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Maude', 'iPhone', 1165);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Savannah', 'Samsung A8', 70);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Delinda', 'Samsung', 25);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Wyatt', 'Nokia 6', 313);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Jarrett', 'Nokia', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Antonio', 'Nokia 3110', 517);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Rocco', 'Samsung Note5', 202);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Doretta', 'Samsung J7 Pro', 573);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Darius', 'Nokia 3110', 312);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Savannah', 'moto x4', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Leana', 'Samsung', 454);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Holley', 'HTC Bolt', 42);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Kathaleen', 'iPhone 6 Plus', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Lenny', 'Nokia 8', 174);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Don', 'Sony Xperia XZ Premium', 233);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Phebe', 'Sony Xperia L1', 558);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Savannah', 'Sony Xperia', 210);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Don', 'Samsung S8+', 220);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Teri', 'iPhone 5s', 538);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Jarrett', 'Samsung J7 Pro', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Pamila', 'Nokia 8', 134);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Annita', 'iPhone 6s', 394);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Gwyneth', 'Samsung', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Antoine', 'Samsung Galaxy J5', 287);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Dale', 'Nokia 3110', 329);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Latonia', 'Nokia 8', 96);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Anthony', 'LG', 302);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Esmeralda', 'Nokia 6', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Burl', 'iPhone 5s', 496);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Margaret', 'Nokia 3110', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Elsa', 'Nokia 6', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Sydney', 'LG G6', 474);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Columbus', 'Nokia', 52);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Josiah', 'Samsung', 54);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Ahmad', 'Samsung S8+', 86);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Hung', 'Sony Xperia R1', 166);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Antonio', 'moto e4 Amazon Prime', 796);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Kathleen', 'Samsung Galaxy J5', 305);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Gregorio', 'moto e4 Amazon Prime', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Doretta', 'Nokia', 493);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Heather', 'Nokia', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Andre', 'moto x4', 587);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Darius', 'iPhone', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Latonia', 'Sony Xperia R1', 52);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Lenny', 'iPhone 6 Plus', 420);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Burl', 'Samsung Note5', 347);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Dana', 'iPhone 5s', 76);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Annita', 'Sony Xperia XZ Premium', 332);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Jermaine', 'Samsung S8+', 330);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Tawna', 'HTC U11', 344);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Fredericka', 'Samsung', 81);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Kathleen', 'Samsung Galaxy J5', 573);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Alexia', 'Sony Xperia XZ Premium', 575);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Katie', 'iPhone 8', 244);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Jene', 'Nokia 6', 594);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Howard', 'LG G6+', 279);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Darius', 'Nokia', 208);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Teri', 'moto e4 Amazon Prime', 537);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Fredericka', 'iPhone 6s', 378);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Tawna', 'Samsung S8+', 13);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Chang', 'Samsung', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Heather', 'Samsung', 71);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Pamila', 'Samsung S8', 361);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Alexia', 'Sony Xperia', 205);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Josiah', 'Nokia 6', 533);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Mose', 'Samsung', 211);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Heather', 'iPhone 5s', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Deloise', 'LG V30', 320);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Kathaleen', 'Nokia', 536);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Hung', 'Samsung S8', 233);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Jarrett', 'Nokia 3110', 415);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Teri', 'Samsung S8+', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Jene', 'LG V30', 12);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Hung', 'LG', 91);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Dana', 'Nokia', 135);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Darnell', 'moto x4', 306);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Genie', 'Samsung Note5', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Pamila', 'Sony Xperia XZ Premium', 182);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Kathleen', 'Nokia 8', 475);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Jene', 'Samsung', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Howard', 'Nokia 3110', 474);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Ahmad', 'Sony Xperia R1', 541);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Henry', 'Samsung', 551);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Pasquale', 'Samsung A8', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Grant', 'LG', 96);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Chang', 'Sony Xperia R1', 393);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Columbus', 'Nokia 3110', 186);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Heather', 'Samsung Galaxy J5', 29);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Linette', 'HTC U11', 562);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Fredericka', 'LG G6+', 493);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Elsa', 'iPhone 8 Plus', 871);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Tad', 'Samsung J7 Pro', 207);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Josiah', 'LG V30+', 725);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Jene', 'Samsung Note5', 245);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Keneth', 'Sony Xperia R1', 595);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Jacques', 'LG', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Jene', 'iPhone 6', 249);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Genie', 'Samsung S8', 19);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Chang', 'moto e4 Amazon Prime', 350);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Lelah', 'iPhone', 98);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Ahmad', 'moto e4 Amazon Prime', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Avelina', 'LG G6', 66);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Maude', 'Nokia 3110', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Darius', 'LG V30+', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Cyril', 'moto x4 Android One', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Darius', 'Sony Xperia L1', 252);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Katie', 'Samsung', 215);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Keneth', 'LG G6', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Genie', 'Samsung Galaxy J5', 129);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Phebe', 'LG', 87);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Gwyneth', 'LG V30+', 475);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Kathleen', 'Sony Xperia R1', 527);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Grant', 'iPhone 6', 99);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Elinore', 'Nokia 3110', 25);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Ahmad', 'Nokia 3110', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Katie', 'iPhone', 435);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Alexia', 'LG', 222);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Ivory', 'Sony Xperia XZ Premium', 114);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Avelina', 'Samsung S8+', 213);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Leana', 'LG', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Savannah', 'iPhone 6s', 587);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Latonia', 'Nokia 3110', 1149);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Cuc', 'Nokia 6', 343);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Jess', 'moto x4 Android One', 140);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Julianna', 'Samsung A8', 248);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Dale', 'Samsung', 1199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Henry', 'iPhone X', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Ahmad', 'LG', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Avelina', 'moto x4 Android One', 364);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Katie', 'LG G6', 19);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Wyatt', 'LG G6', 474);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Frederick', 'Nokia 3110', 558);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Delinda', 'iPhone 6 Plus', 180);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Antonio', 'LG G6', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Ivory', 'moto e4 Amazon Prime', 156);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Mignon', 'HTC U Ultra', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Teri', 'iPhone', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Genie', 'iPhone 5', 107);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Delinda', 'Nokia 8', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Caryl', 'Samsung A8', 45);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Fredericka', 'Samsung Galaxy J5', 234);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Don', 'Nokia 8', 568);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Delinda', 'Samsung', 177);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Holley', 'Samsung', 59);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Asha', 'LG', 289);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Tad', 'HTC U11 life', 1034);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Leana', 'HTC Bolt', 57);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Antonio', 'Sony Xperia L1', 60);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Gregorio', 'LG', 516);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Keneth', 'iPhone 5', 486);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Columbus', 'iPhone 8', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Keneth', 'Sony Xperia L1', 427);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Hung', 'Sony Xperia', 416);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Claud', 'Nokia 8', 255);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Chang', 'Samsung J7 Pro', 340);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Fredericka', 'iPhone 5s', 199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Lenny', 'Sony Xperia L1', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Dana', 'LG G6', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Otis', 'Samsung Galaxy J5', 566);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Wyatt', 'LG G6', 351);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Julianna', 'iPhone', 330);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Josiah', 'LG G6+', 326);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Rocco', 'HTC U11', 184);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Fredericka', 'Nokia 8', 622);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Maryann', 'moto e4 Amazon Prime', 315);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Teri', 'LG', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Holley', 'moto x4 Android One', 483);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Latonia', 'Sony Xperia', 47);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Holley', 'Samsung J7 Pro', 239);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Shantel', 'Sony Xperia', 246);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Andre', 'Samsung S8+', 23);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Jarrett', 'LG G6+', 96);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Lenny', 'Nokia', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Burl', 'iPhone 8 Plus', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Avelina', 'HTC U11', 342);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Heather', 'iPhone X', 103);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Caryl', 'iPhone 6 Plus', 156);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Pamila', 'LG V30', 542);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Burl', 'HTC Bolt', 436);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Leana', 'Nokia 3110', 578);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Savannah', 'iPhone', 176);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Wyatt', 'iPhone 6s', 211);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Elsa', 'Sony Xperia L1', 59);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Fredericka', 'Nokia 3110', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Josiah', 'Sony Xperia L1', 456);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Jacques', 'moto x4 Android One', 76);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Teri', 'Nokia 3110', 102);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Lenny', 'moto x4', 429);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Pamila', 'LG', 217);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Anthony', 'Nokia', 397);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Cole', 'Nokia', 142);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Kandace', 'Nokia 8', 471);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Tyrone', 'Samsung', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Ahmad', 'HTC Bolt', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Tamar', 'LG V30', 596);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Henry', 'LG', 549);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Holley', 'Samsung S8+', 577);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Elinore', 'HTC Bolt', 124);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Mignon', 'moto e4 Amazon Prime', 589);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Hung', 'iPhone 6 Plus', 94);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Heather', 'moto e4 Amazon Prime', 241);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Holley', 'Samsung S8', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Kathaleen', 'Samsung', 252);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Kathleen', 'HTC Bolt', 162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Darius', 'iPhone', 85);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Jacques', 'Nokia 6', 785);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Wyatt', 'Nokia 3110', 46);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Jarrett', 'Nokia 3110', 27);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Jacques', 'LG', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Alison', 'LG G6', 584);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Frederick', 'iPhone 5', 512);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Delinda', 'moto e4 Amazon Prime', 537);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Leana', 'moto x4', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Cuc', 'Samsung', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Doretta', 'HTC U11', 302);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Darius', 'Samsung Note5', 123);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Grant', 'Sony Xperia L1', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Linette', 'Nokia', 210);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Elsa', 'Samsung S8+', 117);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Shantel', 'Samsung S8', 383);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Tyrone', 'LG V30+', 245);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Freddy', 'Samsung Note5', 264);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Lenny', 'Nokia 6', 577);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Cole', 'Samsung Note5', 256);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Freddy', 'HTC U Ultra', 127);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Tamar', 'Samsung', 498);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Jene', 'iPhone 6 Plus', 929);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Esmeralda', 'Samsung S8+', 301);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Cole', 'iPhone 6 Plus', 594);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Tad', 'Sony Xperia L1', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Teri', 'Nokia 3110', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Josiah', 'Samsung Galaxy J5', 460);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Ivory', 'iPhone', 512);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Genie', 'HTC Bolt', 600);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Linette', 'Nokia', 485);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Tad', 'iPhone X', 487);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Pasquale', 'HTC U11 life', 336);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Ivory', 'Samsung J7 Pro', 494);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Darius', 'HTC Bolt', 567);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Ivory', 'Nokia', 521);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Edwardo', 'iPhone 6 Plus', 28);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Asha', 'Sony Xperia XZ Premium', 14);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Jarrett', 'iPhone', 213);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Elinore', 'Samsung Galaxy J5', 95);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Claud', 'iPhone 5', 184);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Heather', 'Samsung', 551);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Latonia', 'iPhone', 1119);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Genie', 'HTC U11', 67);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Frederick', 'moto x4 Android One', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Lelah', 'LG V30+', 47);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Avelina', 'Nokia', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Delinda', 'LG G6+', 227);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Deloise', 'moto e4 Amazon Prime', 538);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Holley', 'Nokia 3110', 361);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Sydney', 'iPhone 6s', 99);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Darius', 'iPhone 5s', 36);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Josiah', 'iPhone X', 562);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Maryann', 'Sony Xperia', 123);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Maryann', 'Samsung Galaxy J5', 386);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Fredericka', 'LG V30+', 478);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Keneth', 'Nokia', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Jacques', 'Samsung A8', 522);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Cyril', 'Samsung Note5', 260);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Andreas', 'LG V30+', 220);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Alison', 'iPhone X', 730);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Rocco', 'moto x4 Android One', 214);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Cole', 'Samsung Note5', 276);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Cuc', 'Sony Xperia XZ Premium', 572);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Charlsie', 'moto x4 Android One', 292);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Dale', 'Nokia 3110', 155);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Kathleen', 'HTC Bolt', 338);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Chang', 'Sony Xperia', 285);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Heather', 'Nokia 3110', 67);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Andre', 'Samsung', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Otis', 'LG G6+', 337);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Fredericka', 'LG G6', 380);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Gwyneth', 'Sony Xperia R1', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Keneth', 'Samsung A8', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Jarrett', 'HTC U11', 34);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Josiah', 'Nokia 6', 935);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Gemma', 'Samsung Note5', 252);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Otis', 'Nokia', 147);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Antonio', 'iPhone 6 Plus', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Kathaleen', 'Samsung Galaxy J5', 112);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Kathleen', 'Samsung A8', 1168);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Andreas', 'Nokia 8', 273);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Kathaleen', 'LG G6', 436);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Roseline', 'LG V30', 412);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Antonio', 'LG G6+', 380);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Andreas', 'iPhone', 360);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Asha', 'HTC Bolt', 221);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Fredericka', 'Nokia 8', 27);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Esmeralda', 'iPhone 6s', 424);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Jess', 'Nokia 3110', 43);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Kathaleen', 'iPhone', 502);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Columbus', 'iPhone X', 470);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Lenny', 'LG V30', 151);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Darius', 'Samsung Galaxy J5', 615);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Burl', 'LG', 410);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Linette', 'moto e4 Amazon Prime', 442);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Lelah', 'Nokia', 455);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Claud', 'Samsung A8', 457);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Margaret', 'moto x4', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Maryann', 'Samsung', 153);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Edwardo', 'Sony Xperia', 189);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Keneth', 'LG G6', 189);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Tawna', 'Samsung Note5', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Heather', 'Nokia 3110', 289);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Jacques', 'HTC Bolt', 562);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Pasquale', 'Samsung', 44);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Dale', 'Samsung S8+', 262);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Jermaine', 'Samsung J7 Pro', 176);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Tad', 'Sony Xperia L1', 313);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Jermaine', 'Nokia 3110', 276);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Caryl', 'Nokia', 151);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Elsa', 'Sony Xperia R1', 185);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Cuc', 'Nokia', 108);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Leana', 'Samsung Note5', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Deloise', 'Nokia', 34);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Darius', 'Samsung Note5', 556);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Fredericka', 'Nokia 3110', 579);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Freddy', 'Nokia', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Leana', 'LG V30', 54);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Ahmad', 'Samsung', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Holley', 'iPhone 8', 570);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Phebe', 'Nokia 8', 151);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Columbus', 'HTC U Ultra', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Caryl', 'Samsung', 490);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Caryl', 'moto e4 Amazon Prime', 295);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Roseline', 'LG G6+', 544);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Kathleen', 'HTC U11', 249);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Josiah', 'Samsung', 574);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Tyrone', 'iPhone 5s', 429);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Doretta', 'moto e4 Amazon Prime', 305);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Cole', 'Nokia', 504);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Andre', 'Samsung A8', 651);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Josiah', 'Sony Xperia XZ Premium', 568);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Jermaine', 'iPhone', 482);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Pamila', 'Nokia', 451);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Kandace', 'moto x4 Android One', 50);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Margaret', 'Samsung A8', 484);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Holley', 'iPhone X', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Alexia', 'Samsung A8', 266);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Delinda', 'Samsung Galaxy J5', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Elinore', 'LG G6', 297);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Elinore', 'Nokia 8', 156);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Burl', 'Nokia', 428);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Rocco', 'Samsung S8', 480);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Henry', 'moto x4', 640);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Cyril', 'Nokia 3110', 222);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Howard', 'Nokia 8', 479);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Margaret', 'Samsung Galaxy J5', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Avelina', 'Nokia', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Wyatt', 'Samsung A8', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Shantel', 'Samsung A8', 507);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Mose', 'Samsung S8+', 227);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Roseline', 'Sony Xperia', 455);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Tamar', 'LG G6', 33);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Jene', 'Samsung A8', 83);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Mignon', 'iPhone X', 289);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Anthony', 'Sony Xperia R1', 479);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Gregorio', 'moto x4 Android One', 270);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Howard', 'iPhone X', 499);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Claud', 'Samsung J7 Pro', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Tyrone', 'HTC Bolt', 408);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Elinore', 'Nokia 3110', 108);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Kathleen', 'Samsung A8', 613);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Henry', 'moto e4 Amazon Prime', 199);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Columbus', 'iPhone 5', 10);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Shantel', 'HTC U11', 127);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Holley', 'iPhone 6s', 35);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Kathleen', 'LG G6+', 291);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Maude', 'Samsung Note5', 456);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Anthony', 'iPhone 8', 168);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Esmeralda', 'moto x4 Android One', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Ahmad', 'Samsung J7 Pro', 398);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Burl', 'Nokia', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Wyatt', 'HTC U11 life', 402);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Mignon', 'HTC Bolt', 178);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Cuc', 'HTC U11', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Andreas', 'Samsung Note5', 74);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Elsa', 'iPhone 6s', 417);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Phebe', 'HTC U11', 312);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Annita', 'moto x4', 103);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Pasquale', 'Samsung S8+', 292);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Heather', 'LG', 272);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Henry', 'moto x4 Android One', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Savannah', 'Samsung Galaxy J5', 90);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Elinore', 'Samsung S8+', 541);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Darius', 'LG G6', 456);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Grant', 'Nokia 8', 479);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Jarrett', 'Sony Xperia', 469);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Esmeralda', 'HTC Bolt', 589);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Wyatt', 'HTC Bolt', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Elinore', 'Sony Xperia R1', 173);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Cyril', 'Samsung', 427);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Delinda', 'Nokia', 354);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Maude', 'HTC U11 life', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Darnell', 'Nokia 6', 301);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Jarrett', 'HTC U11', 598);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Alison', 'LG G6+', 450);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Gwyneth', 'Sony Xperia R1', 544);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Chang', 'iPhone', 191);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Maude', 'Sony Xperia R1', 147);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Latonia', 'moto e4 Amazon Prime', 297);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Elsa', 'iPhone 5', 263);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Leana', 'Samsung J7 Pro', 170);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Margaret', 'Nokia 6', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Holley', 'Samsung A8', 398);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Chang', 'HTC Bolt', 453);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Lenny', 'iPhone 8', 486);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Doretta', 'moto x4 Android One', 600);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Burl', 'LG', 407);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Kathleen', 'moto e4 Amazon Prime', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Mignon', 'Samsung S8+', 259);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Doretta', 'LG', 114);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Cole', 'LG', 511);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Leana', 'moto e4 Amazon Prime', 584);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Elinore', 'Nokia 6', 182);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Tyrone', 'LG V30', 529);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Doretta', 'iPhone 5', 481);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Josiah', 'Samsung', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Alexia', 'Samsung', 483);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Maude', 'iPhone X', 485);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Jess', 'Samsung A8', 500);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Fredericka', 'LG G6', 357);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Julianna', 'Samsung', 562);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Holley', 'Samsung A8', 552);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Ivory', 'iPhone X', 179);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Roseline', 'Samsung S8+', 346);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Caryl', 'Samsung J7 Pro', 67);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Margaret', 'Nokia 8', 298);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Columbus', 'iPhone 8', 519);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Andre', 'Nokia 3110', 240);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Kathleen', 'iPhone 6s', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Dana', 'Samsung A8', 32);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Jene', 'Sony Xperia XZ Premium', 89);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Don', 'Samsung A8', 705);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Jene', 'iPhone', 1187);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Chang', 'HTC Bolt', 176);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Elsa', 'Nokia 6', 196);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Deloise', 'Nokia 3110', 112);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Chang', 'HTC U11', 60);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Claud', 'iPhone 8 Plus', 161);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Lenny', 'HTC U11 life', 342);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Alexia', 'Samsung A8', 362);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Dale', 'moto e4 Amazon Prime', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Fredericka', 'Nokia', 332);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Deloise', 'moto x4 Android One', 442);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Cyril', 'iPhone 6s', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Savannah', 'Sony Xperia R1', 75);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Dale', 'iPhone 6 Plus', 414);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Ivory', 'Samsung', 283);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Ahmad', 'moto e4 Amazon Prime', 247);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Linette', 'LG', 93);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Frederick', 'Samsung Note5', 101);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Pasquale', 'Samsung Note5', 307);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Don', 'Sony Xperia L1', 213);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Alexia', 'Nokia 6', 131);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Cyril', 'Samsung A8', 506);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Linette', 'Samsung Note5', 437);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Don', 'moto x4', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Genie', 'Samsung S8+', 437);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Dana', 'Sony Xperia', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Heather', 'LG G6', 155);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Antoine', 'Samsung A8', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Ivory', 'iPhone 6s', 420);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Cole', 'Nokia', 121);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Cuc', 'HTC U Ultra', 530);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Tad', 'HTC U Ultra', 231);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Jene', 'iPhone 6s', 515);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Phebe', 'LG G6', 51);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Wyatt', 'iPhone', 525);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Mignon', 'iPhone 5', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Elsa', 'iPhone 6', 62);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Avelina', 'Nokia 8', 263);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Dale', 'HTC U11', 204);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Caryl', 'moto x4 Android One', 448);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Heather', 'LG V30+', 97);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Elsa', 'Sony Xperia L1', 224);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Don', 'Samsung A8', 224);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Doretta', 'Sony Xperia', 491);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Mignon', 'moto e4 Amazon Prime', 246);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Claud', 'LG V30+', 70);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Burl', 'Samsung Note5', 31);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Columbus', 'Sony Xperia XZ Premium', 166);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Mignon', 'HTC Bolt', 53);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Howard', 'Samsung A8', 351);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Darnell', 'HTC U11 life', 473);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Dale', 'Nokia 8', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Jess', 'moto x4 Android One', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Rocco', 'HTC U Ultra', 568);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Phebe', 'Nokia', 18);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Wyatt', 'iPhone 5s', 272);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Linette', 'moto x4', 233);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Claud', 'Samsung S8', 236);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Esmeralda', 'LG G6', 217);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Jene', 'Sony Xperia', 463);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Antonio', 'Sony Xperia', 353);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Roseline', 'iPhone 8 Plus', 92);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Jermaine', 'HTC U Ultra', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Alexia', 'Nokia', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Jess', 'moto x4', 356);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Phebe', 'Samsung A8', 98);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Doretta', 'Sony Xperia L1', 243);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Pasquale', 'Samsung', 97);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Doretta', 'HTC U Ultra', 281);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Fredericka', 'Samsung A8', 552);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Rocco', 'LG G6+', 172);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Annita', 'Sony Xperia L1', 412);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Edwardo', 'moto x4 Android One', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Deloise', 'Samsung S8+', 599);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Antoine', 'LG', 238);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Doretta', 'iPhone 5', 94);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Anthony', 'Samsung S8+', 234);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Mignon', 'Samsung A8', 90);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Avelina', 'Sony Xperia XZ Premium', 293);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Cuc', 'iPhone 6s', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Cyril', 'Nokia 3110', 510);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Jene', 'iPhone', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Phebe', 'iPhone 6s', 461);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Dale', 'iPhone', 449);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Julianna', 'Samsung S8', 385);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Fredericka', 'HTC U Ultra', 188);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Savannah', 'LG V30', 538);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Darnell', 'moto e4 Amazon Prime', 203);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Keneth', 'HTC U Ultra', 98);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Maude', 'Sony Xperia', 595);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Sydney', 'LG V30+', 343);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Latonia', 'moto e4 Amazon Prime', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Hung', 'Samsung Note5', 348);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Kathleen', 'Samsung Galaxy J5', 101);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Antoine', 'moto x4 Android One', 229);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Chang', 'Nokia 6', 573);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Jermaine', 'iPhone 6s', 130);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Chang', 'Nokia 3110', 24);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Lelah', 'HTC Bolt', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Rocco', 'LG', 134);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Josiah', 'Samsung', 515);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Savannah', 'moto e4 Amazon Prime', 57);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Cyril', 'HTC Bolt', 478);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Keneth', 'Samsung S8+', 108);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Chang', 'iPhone', 461);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Avelina', 'HTC Bolt', 546);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Annita', 'Nokia 8', 397);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Deloise', 'moto x4 Android One', 507);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Doretta', 'Nokia 8', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Edwardo', 'iPhone 8 Plus', 396);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Lenny', 'Sony Xperia XZ Premium', 414);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Shantel', 'Nokia 3110', 189);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Josiah', 'Samsung S8+', 360);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Howard', 'LG G6', 564);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Kathaleen', 'LG V30+', 72);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Anthony', 'HTC U Ultra', 99);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Mose', 'Samsung A8', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Andre', 'Nokia 3110', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Antoine', 'Samsung', 298);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Antoine', 'LG G6', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Lenny', 'Nokia', 38);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Shantel', 'Samsung', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Claud', 'moto e4 Amazon Prime', 367);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Charlsie', 'moto x4 Android One', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Dana', 'Sony Xperia', 357);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Leana', 'Samsung', 356);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Lelah', 'LG', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Henry', 'iPhone 5', 41);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Julianna', 'iPhone 5s', 221);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Shantel', 'iPhone 8 Plus', 226);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Sydney', 'moto e4 Amazon Prime', 354);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Genie', 'LG V30+', 216);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Lenny', 'Nokia', 442);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Pamila', 'iPhone 6s', 428);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Alison', 'Samsung A8', 597);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Chang', 'LG V30+', 563);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Dale', 'iPhone 6s', 420);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Kathaleen', 'Samsung A8', 52);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Jene', 'iPhone', 598);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Claud', 'iPhone', 319);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Jacques', 'iPhone X', 165);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Anthony', 'Samsung A8', 195);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Freddy', 'Nokia', 461);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Roseline', 'Nokia', 119);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Anthony', 'moto e4 Amazon Prime', 536);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Jacques', 'moto e4 Amazon Prime', 567);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Otis', 'LG V30', 17);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Tamar', 'LG', 573);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Alexia', 'moto x4', 494);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Mignon', 'iPhone 5s', 467);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Heather', 'LG V30+', 660);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Mignon', 'Nokia 3110', 480);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Wyatt', 'Nokia 6', 120);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Gwyneth', 'Nokia', 248);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Ahmad', 'iPhone 5', 524);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Tad', 'LG V30+', 414);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Tamar', 'HTC U Ultra', 499);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Mignon', 'Samsung', 416);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Julianna', 'Samsung J7 Pro', 485);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Andreas', 'Nokia', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Maude', 'Nokia 3110', 186);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Josiah', 'Samsung S8', 839);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Tawna', 'Sony Xperia', 274);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Jermaine', 'moto x4 Android One', 454);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Dale', 'LG V30', 19);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Tamar', 'iPhone 6', 524);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Linette', 'iPhone', 788);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Gregorio', 'Samsung J7 Pro', 209);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Otis', 'iPhone 5s', 206);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Deloise', 'Nokia', 171);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Caryl', 'iPhone', 55);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Hung', 'Sony Xperia L1', 333);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Charlsie', 'LG G6+', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Gwyneth', 'Sony Xperia R1', 216);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Maude', 'Sony Xperia', 551);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Jarrett', 'iPhone 6', 225);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Roseline', 'moto x4 Android One', 561);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Jene', 'Samsung', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Jermaine', 'LG G6', 580);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Grant', 'HTC U11 life', 447);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Pamila', 'Samsung', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Pasquale', 'Samsung A8', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Tyrone', 'LG G6', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Teri', 'Tyrone', 'HTC U Ultra', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Edwardo', 'Samsung A8', 205);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Otis', 'Nokia', 132);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Kandace', 'moto e4 Amazon Prime', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Grant', 'Sony Xperia', 152);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Shantel', 'Nokia 3110', 587);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Esmeralda', 'Sony Xperia', 520);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Frederick', 'Samsung', 499);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Margaret', 'Nokia', 347);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Gregorio', 'Sony Xperia L1', 132);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Linette', 'HTC U11 life', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Tad', 'Samsung A8', 237);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Mignon', 'Nokia 3110', 593);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Gwyneth', 'iPhone 6s', 63);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Caryl', 'moto e4 Amazon Prime', 136);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Otis', 'LG V30+', 409);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Leana', 'iPhone', 218);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Teri', 'iPhone 8', 36);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Holley', 'Sony Xperia R1', 430);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Frederick', 'Nokia 6', 463);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Cyril', 'Nokia 6', 118);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Charlsie', 'iPhone', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Mignon', 'LG', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Antonio', 'HTC Bolt', 157);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Sydney', 'iPhone 6s', 128);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Henry', 'Sony Xperia', 230);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Dale', 'iPhone 5s', 374);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Jess', 'LG G6', 365);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Cole', 'iPhone 5s', 12);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Anthony', 'Nokia 3110', 127);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Latonia', 'iPhone', 294);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Dale', 'iPhone 5', 416);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Jene', 'HTC U Ultra', 524);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Dana', 'Nokia', 19);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Chang', 'Samsung', 161);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Chang', 'Nokia 3110', 371);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Asha', 'Samsung J7 Pro', 586);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Heather', 'Nokia 3110', 104);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Gregorio', 'iPhone X', 537);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Savannah', 'Nokia 6', 329);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Avelina', 'iPhone', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Chang', 'iPhone 6 Plus', 231);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Burl', 'Samsung Galaxy J5', 143);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Lelah', 'iPhone', 483);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Kandace', 'iPhone 6s', 513);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Elinore', 'Samsung', 31);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Holley', 'Nokia 3110', 344);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Dana', 'iPhone X', 159);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Kathaleen', 'Sony Xperia', 232);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Maude', 'Edwardo', 'iPhone 5s', 57);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Pasquale', 'HTC U11', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Don', 'Samsung', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Katie', 'Sony Xperia', 435);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Cyril', 'Sony Xperia XZ Premium', 198);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Frederick', 'Samsung', 1157);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Tyrone', 'HTC Bolt', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Linette', 'Nokia 8', 331);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Doretta', 'iPhone 5s', 271);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Deloise', 'LG G6', 58);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Jess', 'HTC Bolt', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Annita', 'moto x4 Android One', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Elinore', 'iPhone', 577);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Phebe', 'Samsung A8', 129);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Sydney', 'Samsung S8+', 15);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Freddy', 'Samsung A8', 300);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Savannah', 'iPhone X', 468);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Dana', 'iPhone', 113);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Dana', 'LG G6+', 145);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Anthony', 'Ahmad', 'Sony Xperia', 94);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Fredericka', 'Samsung Note5', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Maude', 'iPhone 6 Plus', 536);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Howard', 'iPhone', 309);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Wyatt', 'HTC U11', 545);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Elsa', 'moto e4 Amazon Prime', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Pasquale', 'Jermaine', 'HTC Bolt', 562);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Doretta', 'Samsung Note5', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Don', 'LG', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Linette', 'Nokia', 300);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Ivory', 'LG V30+', 243);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Tawna', 'moto x4 Android One', 210);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Jene', 'LG G6+', 405);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Darnell', 'Samsung', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Chang', 'HTC U11 life', 561);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Tyrone', 'Nokia 3110', 14);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Jarrett', 'moto e4 Amazon Prime', 335);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Doretta', 'Samsung Note5', 208);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Tawna', 'moto e4 Amazon Prime', 303);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Latonia', 'moto x4 Android One', 155);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Neal', 'Antonio', 'Nokia 3110', 1195);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Lenny', 'Nokia 3110', 51);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Tad', 'Samsung J7 Pro', 71);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Antonio', 'moto x4 Android One', 127);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Chang', 'Nokia 6', 541);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Ahmad', 'Samsung A8', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Columbus', 'Rocco', 'Sony Xperia', 101);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Lelah', 'Sony Xperia R1', 344);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Gemma', 'Sony Xperia XZ Premium', 177);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Otis', 'LG V30', 582);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Mignon', 'iPhone', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Jarrett', 'moto e4 Amazon Prime', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Jermaine', 'Samsung Note5', 162);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Darnell', 'LG', 788);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Pamila', 'HTC Bolt', 284);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Tamar', 'Nokia 3110', 336);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Fredericka', 'iPhone 5', 133);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lillie', 'Tawna', 'moto e4 Amazon Prime', 426);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Alison', 'iPhone', 587);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Lelah', 'Maude', 'HTC Bolt', 342);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Shantel', 'Samsung', 314);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jacques', 'Julianna', 'Nokia 6', 158);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Grant', 'Samsung Note5', 290);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Heather', 'Samsung S8', 169);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Delinda', 'Samsung', 576);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Latonia', 'Sony Xperia R1', 419);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Don', 'Nokia 6', 338);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Edwardo', 'iPhone 8 Plus', 522);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Kandace', 'iPhone 6s', 411);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Keneth', 'Nokia 3110', 289);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Tamar', 'HTC U11 life', 454);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Claud', 'iPhone 5s', 379);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Columbus', 'LG', 129);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Teri', 'HTC Bolt', 176);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Henry', 'Sony Xperia R1', 377);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Don', 'LG V30+', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Delinda', 'Nokia 3110', 247);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Ahmad', 'Nokia 6', 411);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Kandace', 'Samsung', 435);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Darius', 'Nokia 3110', 501);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Charlsie', 'Samsung Note5', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Tyrone', 'Samsung', 346);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Doretta', 'Mose', 'iPhone', 266);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Burl', 'Sony Xperia', 415);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Avelina', 'iPhone X', 244);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Cole', 'Samsung Note5', 198);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Tyrone', 'iPhone 5', 253);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Avelina', 'LG', 591);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Pasquale', 'Samsung Note5', 569);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Frederick', 'LG', 310);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Jacques', 'moto x4 Android One', 302);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Tamar', 'Nokia 6', 400);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Holley', 'Samsung Note5', 238);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Alexia', 'HTC U11 life', 372);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Burl', 'moto x4 Android One', 415);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delsie', 'Grant', 'iPhone X', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Kathaleen', 'iPhone X', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kandace', 'Lenny', 'Samsung S8+', 126);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Antoine', 'iPhone 6', 381);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Frederick', 'iPhone 6', 545);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Tawna', 'Samsung A8', 521);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Roseline', 'iPhone 6', 351);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Darnell', 'LG V30+', 352);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Andre', 'Samsung S8', 340);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Leandro', 'iPhone', 259);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Teri', 'moto x4 Android One', 451);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Claud', 'moto e4 Amazon Prime', 181);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Cuc', 'LG', 237);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Doretta', 'Nokia 3110', 479);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Alison', 'moto x4', 618);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kathaleen', 'Mignon', 'Nokia 3110', 385);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Lelah', 'Nokia 8', 563);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Darnell', 'HTC U Ultra', 56);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Howard', 'LG V30', 13);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leana', 'Don', 'iPhone 5', 107);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Pamila', 'Samsung S8+', 187);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Darius', 'iPhone X', 486);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jaime', 'Burl', 'Nokia 3110', 398);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Mose', 'LG', 328);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Leandro', 'iPhone X', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Maryann', 'Samsung Note5', 223);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Cyril', 'moto e4 Amazon Prime', 61);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deon', 'Lelah', 'Nokia 8', 290);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Deloise', 'Tad', 'iPhone X', 338);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Chang', 'Lelah', 'HTC U11 life', 803);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Chang', 'Nokia 3110', 532);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Otis', 'Burl', 'moto e4 Amazon Prime', 76);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Elinore', 'HTC U Ultra', 112);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Suzie', 'Shantel', 'Sony Xperia', 446);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Josiah', 'Sony Xperia L1', 97);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Maryann', 'Samsung Note5', 194);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ahmad', 'Genie', 'iPhone 5', 77);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Grant', 'Samsung S8+', 23);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Claud', 'LG G6+', 540);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Phebe', 'LG', 33);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Tawna', 'Nokia 3110', 571);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Richard', 'Julianna', 'Nokia 8', 464);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Caryl', 'iPhone 6s', 277);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Essie', 'Pasquale', 'LG', 68);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Hung', 'moto x4 Android One', 588);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Kathleen', 'moto e4 Amazon Prime', 359);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Heather', 'Samsung Galaxy J5', 164);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Andre', 'Jene', 'Nokia 3110', 476);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Pasquale', 'moto e4 Amazon Prime', 469);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Pamila', 'LG G6', 112);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Annita', 'Freddy', 'Samsung A8', 565);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Shantel', 'Sony Xperia R1', 388);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Andreas', 'HTC Bolt', 367);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Gregorio', 'iPhone X', 33);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dale', 'Teri', 'iPhone', 94);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Henry', 'Samsung S8', 404);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Margaret', 'iPhone', 228);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Raguel', 'Mignon', 'iPhone 5', 129);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Gregorio', 'iPhone 6', 197);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Tamar', 'Samsung', 1040);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Freddy', 'Nokia 6', 317);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sydney', 'Annita', 'Samsung Note5', 238);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Andreas', 'Samsung Note5', 105);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Caryl', 'Burl', 'moto x4', 55);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Teri', 'Nokia', 26);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Grant', 'Nokia', 239);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Claud', 'Samsung', 298);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Keneth', 'Samsung J7 Pro', 511);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Wyatt', 'LG G6', 433);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Dana', 'Cuc', 'LG', 360);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Sydney', 'Samsung Note5', 208);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Sanford', 'Teri', 'iPhone 6 Plus', 136);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Maude', 'Samsung Note5', 342);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alexia', 'Jess', 'HTC Bolt', 313);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Ivory', 'Samsung A8', 215);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Caryl', 'iPhone', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jermaine', 'Phebe', 'Nokia 6', 148);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cole', 'Columbus', 'HTC U11', 91);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mose', 'Pamila', 'Samsung A8', 212);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Mignon', 'Tamar', 'iPhone 6s', 584);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gregorio', 'Cyril', 'Samsung Note5', 403);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Latonia', 'Gregorio', 'Samsung A8', 167);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Julianna', 'Tyrone', 'LG G6', 508);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Sydney', 'moto x4', 250);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Kathaleen', 'Samsung', 547);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Frederick', 'Samsung S8+', 308);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Sydney', 'LG G6', 481);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Tamar', 'Samsung A8', 323);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Elsa', 'Nokia 3110', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ramon', 'Alexia', 'LG', 251);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Phebe', 'LG G6', 197);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Shantel', 'Lelah', 'Nokia 3110', 268);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darnell', 'Andreas', 'Samsung Note5', 12);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Howard', 'iPhone 6s', 531);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Ivory', 'Jene', 'Sony Xperia R1', 465);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Andre', 'Sony Xperia XZ Premium', 184);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Savannah', 'Sony Xperia', 497);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Rocco', 'Elsa', 'Samsung J7 Pro', 388);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roselle', 'Asha', 'Samsung', 178);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Hung', 'Asha', 'LG V30+', 132);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Phebe', 'Gregorio', 'Sony Xperia', 438);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Margaret', 'Tawna', 'LG', 69);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Otis', 'LG', 528);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Pamila', 'Nokia 8', 418);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Leandro', 'Samsung', 577);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Katie', 'Sony Xperia', 599);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Kristie', 'Claud', 'Sony Xperia L1', 191);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Maryann', 'Samsung Galaxy J5', 544);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Delinda', 'Mose', 'Nokia 8', 557);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Katie', 'Sydney', 'LG G6+', 583);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Elinore', 'Kathleen', 'iPhone 5', 550);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Frederick', 'Asha', 'iPhone X', 398);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jene', 'Lelah', 'HTC Bolt', 580);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Keneth', 'Josiah', 'Nokia 3110', 489);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tawna', 'Mose', 'Samsung', 149);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Howard', 'Linette', 'Sony Xperia R1', 282);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Josiah', 'Alison', 'LG V30', 395);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Holley', 'Gemma', 'Samsung Note5', 464);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Don', 'Antoine', 'Nokia 3110', 524);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Gemma', 'Alexia', 'Nokia 3110', 354);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Charlsie', 'Margaret', 'iPhone 6', 560);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Burl', 'Roseline', 'iPhone', 392);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Pamila', 'Nokia', 226);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Reggie', 'Gemma', 'Samsung Note5', 21);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Grant', 'LG', 10);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Alison', 'Jess', 'iPhone 6', 237);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Cuc', 'Caryl', 'HTC U11', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tad', 'Anthony', 'HTC U11', 482);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tamar', 'Linette', 'Samsung Note5', 496);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Leandro', 'Julianna', 'moto e4 Amazon Prime', 477);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antonio', 'Tawna', 'Nokia 8', 321);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Genie', 'Burl', 'HTC U11', 62);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Edwardo', 'Cuc', 'HTC Bolt', 78);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Antoine', 'Kandace', 'moto x4 Android One', 457);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Tyrone', 'Alexia', 'moto x4 Android One', 246);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Roseline', 'Dana', 'moto x4', 72);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Alexia', 'iPhone 5', 409);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Grant', 'Cole', 'Samsung Note5', 425);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Darius', 'Wyatt', 'HTC U11', 559);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Linette', 'Darius', 'moto x4 Android One', 68);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jeffrey', 'Otis', 'Samsung', 345);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Freddy', 'Anthony', 'iPhone', 213);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Wyatt', 'Maude', 'iPhone 5s', 270);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Synthia', 'Columbus', 'iPhone 6s', 123);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Jess', 'Pasquale', 'HTC Bolt', 506);
INSERT INTO Phonecalls(call_from, call_to, phone_type, length)
VALUES ('Avelina', 'Tyrone', 'HTC U Ultra', 301);
