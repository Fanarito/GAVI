import csv

# Setup consumers
import consumers

cons = [
    consumers.TotalCostConsumer(),
    consumers.DepartureDestinationPairsConsumer(),
    consumers.HighestPayerConsumer(),
    consumers.ArthurDentAwardConsumer(),
    consumers.AveragePriceTatooineToAlderanConsumer(),
    consumers.TravellingCompanionsConsumer(),
    consumers.MostPopularRoundTripConsumer(),
    consumers.MostPopularDestinationConsumer(),
    consumers.MostPopularSwappersConsumer(),
    consumers.MostPopularSpaceshipConsumer(),
    consumers.MostExpensiveSpaceshipConsumer(),
    consumers.LargestPriceVariationConsumer(),
    consumers.TheRestlessOneConsumer(),
    consumers.MostFrequentPhoenixTravellerConsumer(),
    consumers.HighestPlanetPriceVarianceConsumer(),
]

# Read csv file
filename = 'travel_log.csv'
with open(filename) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        for consumer in cons:
            consumer.consume(row)

    for idx, consumer in enumerate(cons):
        print('{}: {}'.format(idx + 1, consumer.display()))
