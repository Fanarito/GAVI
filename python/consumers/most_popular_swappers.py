class MostPopularSwappersConsumer:
    def __init__(self):
        self.travellers = {}

    def consume(self, row):
        if row['Traveller'] not in self.travellers:
            self.travellers[row['Traveller']] = {'moves': set(), 'possible_swaps': set()}
        self.travellers[row['Traveller']]['moves'].add(row['Start'] + '->' + row['Destination'])
        self.travellers[row['Traveller']]['possible_swaps'].add(row['Destination'] + '->' + row['Start'])

    def display(self):
        most = -1
        names = set()
        for name1, traveller1 in self.travellers.items():
            for name2, traveller2 in self.travellers.items():
                if name1 == name2:
                    continue
                swaps = traveller1['moves'].intersection(traveller2['possible_swaps'])
                swaps_count = len(swaps)
                if swaps_count > most:
                    most = swaps_count
                    names = set()
                    names.add(tuple(sorted((name1, name2))))
                elif swaps_count == most:
                    names.add(tuple(sorted((name1, name2))))
        grouped = sorted(['{} & {}'.format(x, y) for x, y in sorted(names)])
        names_display = ', '.join(grouped)
        return 'Most popular swappers ({} trips): {}'.format(most, names_display)
