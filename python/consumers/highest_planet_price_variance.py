class HighestPlanetPriceVarianceConsumer:
    def __init__(self):
        self.planets = {}

    def consume(self, row):
        if row['Start'] not in self.planets:
            self.planets[row['Start']] = {'departure': 0, 'arrival': 0}

        if row['Destination'] not in self.planets:
            self.planets[row['Destination']] = {'departure': 0, 'arrival': 0}

        self.planets[row['Start']]['departure'] += int(row['Price'])
        self.planets[row['Destination']]['arrival'] += int(row['Price'])

    def display(self):
        highest = -1
        names = []
        for name, cost in self.planets.items():
            variance = cost['arrival'] - cost['departure']
            if variance > highest:
                highest = variance
                names = [name]
            elif variance == highest:
                names.append(name)
        names_display = ', '.join(sorted(names))
        return 'Highest arrival_fee - departure_cost (amount = {}): {}'.format(
            highest,
            names_display)
