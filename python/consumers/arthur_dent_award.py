class ArthurDentAwardConsumer:
    def __init__(self):
        self.hitchhiked = {} # Dict of lists, 0: hitchhiked, 1: total

    def consume(self, row):
        if row['Traveller'] not in self.hitchhiked:
            self.hitchhiked[row['Traveller']] = [0, 0]

        self.hitchhiked[row['Traveller']][1] += 1
        if int(row['Price']) == 0:
            self.hitchhiked[row['Traveller']][0] += 1


    def display(self):
        ratios = {}
        largest = -1
        for name, hitched_total in self.hitchhiked.items():
            ratio = hitched_total[0] / hitched_total[1]
            if ratio > largest:
                largest = ratio
            if ratio not in ratios:
                ratios[ratio] = []
            ratios[ratio].append(name)
        names = ', '.join(sorted(ratios[largest]))
        return 'The Arthur Dent award (hitchhiking ratio = {:.2f}): {}'.format(largest, names)
