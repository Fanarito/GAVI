class TheRestlessOneConsumer:
    def __init__(self):
        self.travellers = {}

    def consume(self, row):
        if row['Traveller'] not in self.travellers:
            self.travellers[row['Traveller']] = set()
        self.travellers[row['Traveller']].add(row['Start'])
        self.travellers[row['Traveller']].add(row['Destination'])

    def display(self):
        most = -1
        names = []
        for name, places in self.travellers.items():
            count = len(places)
            if count > most:
                most = count
                names = [name]
            elif count == most:
                names.append(name)
        names_display = ', '.join(sorted(names))
        return 'The restless one ({} different planets): {}'.format(most, names_display)
