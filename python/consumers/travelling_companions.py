class TravellingCompanionsConsumer:
    def __init__(self):
        self.groups = {}

    def consume(self, row):
        lookup = row['Start'] + row['Destination'] + row['Spaceship']
        if lookup not in self.groups:
            self.groups[lookup] = set()
        self.groups[lookup].add(row['Traveller'])

    def display(self):
        group_displays = []
        for group in self.groups.values():
            if len(group) > 2:
                group_displays.append(' & '.join(sorted(group)))
        display = ', '.join(sorted(group_displays))
        return 'Travelling companions: {}'.format(display)
