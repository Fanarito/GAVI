class HighestPayerConsumer:
    def __init__(self):
        self.travellers = {}

    def consume(self, row):
        if row['Traveller'] not in self.travellers:
            self.travellers[row['Traveller']] = 0
        self.travellers[row['Traveller']] += int(row['Price'])

    def display(self):
        highest = -1
        names = []
        for traveller, total in self.travellers.items():
            if total > highest:
                highest = total
                names = [traveller]
            elif total == highest:
                names.append(traveller)
        names = ', '.join(sorted(names))
        return 'Biggest travel cost (amount = {1}): {0}'.format(names, highest)
