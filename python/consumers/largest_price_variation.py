class LargestPriceVariationConsumer:
    def __init__(self):
        self.travellers = {}

    def consume(self, row):
        if row['Traveller'] not in self.travellers:
            self.travellers[row['Traveller']] = { 'lowest': 999999999, 'highest': -1 }
        price = int(row['Price'])
        traveller = self.travellers[row['Traveller']]
        if traveller['lowest'] > price:
            traveller['lowest'] = price
        if traveller['highest'] < price:
            traveller['highest'] = price


    def display(self):
        largest = -1
        names = []
        for name, traveller in self.travellers.items():
            variation = traveller['highest'] - traveller['lowest']
            if variation > largest:
                largest = variation
                names = [name]
            elif variation == largest:
                names.append(name)
        names_display = ', '.join(sorted(names))
        return 'Largest price variation (difference = {}): {}'.format(largest, names_display)
