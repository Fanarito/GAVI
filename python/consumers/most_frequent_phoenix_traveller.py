class MostFrequentPhoenixTravellerConsumer:
    def __init__(self):
        self.crew = {'Kanan Jarrus': 0, 'Hera Syndulla': 0, 'Sabine Wren': 0,
                     'Ezra Bridger': 0, 'Garazeb "Zeb" Orrelios': 0, 'C1-10P': 0}

    def consume(self, row):
        if row['Traveller'] in self.crew.keys():
            self.crew[row['Traveller']] += 1

    def display(self):
        most = -1
        names = []
        for name, trips in self.crew.items():
            if trips > most:
                most = trips
                names = [name]
            elif trips == most:
                names.append(name)
        names_display = ', '.join(sorted(names))
        return 'Most frequent Phoenix traveller ({} trips): {}'.format(most, names_display)
