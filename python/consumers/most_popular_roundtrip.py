class MostPopularRoundTripConsumer:
    def __init__(self):
        self.roundtrips = {}

    def consume(self, row):
        if row['Start'] == row['Destination']:
            if row['Start'] not in self.roundtrips:
                self.roundtrips[row['Start']] = 0
            self.roundtrips[row['Start']] += 1

    def display(self):
        highest = -1
        most_popular = []
        for roundtrip, trips in self.roundtrips.items():
            if trips > highest:
                highest = trips
                most_popular = [roundtrip]
            elif trips == highest:
                most_popular.append(roundtrip)
        roundtrip_display = ', '.join(['{0} - {0}'.format(x) for x in sorted(most_popular)])
        return 'Most popular roundtrip ({1} trips): {0}'.format(roundtrip_display, highest)
