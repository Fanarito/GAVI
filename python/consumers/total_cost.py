class TotalCostConsumer:
    def __init__(self):
        self.cost = 0

    def consume(self, row):
        self.cost += int(row['Price'])

    def display(self):
        return 'Total travel cost = {}'.format(self.cost)
