class MostExpensiveSpaceshipConsumer:
    def __init__(self):
        self.spaceships = {}

    def consume(self, row):
        if row['Spaceship'] not in self.spaceships:
            self.spaceships[row['Spaceship']] = []
        self.spaceships[row['Spaceship']].append(int(row['Price']))

    def display(self):
        highest_avg = -1
        names = []
        for spaceship, prices in self.spaceships.items():
            avg = sum(prices) / len(prices)
            if avg > highest_avg:
                highest_avg = avg
                names = [spaceship]
            elif avg == highest_avg:
                names.append(spaceship)
        name_display = ', '.join(sorted(names))
        return 'Most expensive spaceship (average price = {:.3f}): {}'.format(highest_avg, name_display)
