class DepartureDestinationPairsConsumer:
    def __init__(self):
        self.departure_places = {}

    def consume(self, row):
        if row['Start'] not in self.departure_places:
            self.departure_places[row['Start']] = set()
        self.departure_places[row['Start']].add(row['Destination'])

    def display(self):
        count = 0
        for destinations in self.departure_places.values():
            count += len(destinations)
        return '{} different types of departure - destination pairs.'.format(count)
