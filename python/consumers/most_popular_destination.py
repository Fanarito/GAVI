class MostPopularDestinationConsumer:
    def __init__(self):
        self.destinations = {}

    def consume(self, row):
        if row['Destination'] not in self.destinations:
            self.destinations[row['Destination']] = 0
        self.destinations[row['Destination']] += 1

    def display(self):
        highest = -1
        most_popular = []
        for destination, trips in self.destinations.items():
            if trips > highest:
                highest = trips
                most_popular = [destination]
            elif trips == highest:
                most_popular.append(destination)
        destination_display = ', '.join(sorted(most_popular))
        return 'Most popular destination ({1} persons): {0}'.format(destination_display, highest)
