class AveragePriceTatooineToAlderanConsumer:
    def __init__(self):
        self.price_sum = 0
        self.count = 0

    def consume(self, row):
        if row['Start'] == 'Tatooine' and row['Destination'] == 'Alderaan':
            self.price_sum += int(row['Price'])
            self.count += 1

    def display(self):
        average = self.price_sum / self.count
        return 'Average price for Tatooine to Alderaan = {}'.format(average)
