class MostPopularSpaceshipConsumer:
    def __init__(self):
        self.spaceships = {}

    def consume(self, row):
        if row['Spaceship'] not in self.spaceships:
            self.spaceships[row['Spaceship']] = 0
        self.spaceships[row['Spaceship']] += 1

    def display(self):
        most_trips = -1
        spaceships = []
        for spaceship, trips in self.spaceships.items():
            if trips > most_trips:
                most_trips = trips
                spaceships = [spaceship]
            elif trips == most_trips:
                spaceships.append(spaceship)
        return 'Most popular spaceship ({} trips): {}'.format(most_trips, ', '.join(spaceships))
